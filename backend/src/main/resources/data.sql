
INSERT INTO public.roles ("id", "name", "display_name", "description", "created_at", "updated_at")
VALUES
  (1, 'TaskManager', 'Менеджер задач', NULL, '2019-12-02 18:11:41', '2019-12-02 18:12:02'),
  (2, 'Administrator', 'Администратор', NULL, '2019-12-02 18:11:46', '2019-12-02 18:12:07'),
  (3, 'TaskPerformer', 'Исполнитель', NULL, '2019-12-02 18:11:51', '2019-12-02 18:12:14');

INSERT INTO public.permissions ("id", "name", "display_name", "created_at", "updated_at")
VALUES
  (2, 'TaskAdd', 'Добавление новых задач', '2019-12-02 18:38:49', NULL),
  (5, 'TaskEdit', 'Редактирование задач', '2019-12-02 18:39:52', NULL),
  (6, 'TaskProcess', 'Исполнение задач', '2019-12-02 18:40:46', NULL),
  (7, 'TaskAllList', 'Просмотр всх задач', '2019-12-02 18:42:04', NULL),
  (8, 'TaskMyList', 'Просмотр своих задач', '2019-12-02 18:42:44', NULL),
  (9, 'UserAdm', 'Назначение прав пользователям', '2019-12-02 18:43:39', NULL),
  (10, 'InboxAllList', 'Просмотр всей входящей почты', '2019-12-02 18:50:18', NULL),
  (11, 'InboxMyList', 'Просмотр своей входящей почты', '2019-12-02 18:51:06', NULL),
  (12, 'InboxAdd', 'Добавление входящей почты', '2019-12-02 18:52:28', NULL),
  (13, 'InboxEdit', 'Изменение входящей почты', '2019-12-02 18:53:19', NULL);

INSERT INTO public.role_permissions ("role_id", "permission_id")
VALUES
  (1, 2),
  (1, 5),
  (1, 6),
  (1, 7),
  (1, 8),
  (1, 10),
  (1, 11),
  (1, 12),
  (1, 13),
  (2, 2),
  (2, 5),
  (2, 6),
  (2, 7),
  (2, 8),
  (2, 9),
  (2, 10),
  (2, 11),
  (2, 12),
  (2, 13),
  (3, 6),
  (3, 8),
  (3, 11);


INSERT INTO public.users ("id", "login", "firstname", "lastname", "fathername", "email", "password", "created_at", "updated_at", "telegram_username", "role_id")
VALUES
  (1, 'IvanovaII', 'Ирина', 'Иванова', 'Ивановна', 'ivanovaii@beldashki.ru', '123', '2019-12-02 18:00:00', NULL, NULL, 1),
  (2, 'PetrovPP', 'Пётр', 'Петров', 'Петрович', 'petrovpp@beldashki.ru', '123', '2019-12-02 18:05:00', NULL, NULL, 2),
  (3, 'SemenovSS', 'Семён', 'Семёнов', 'Семёнович', 'semenovss@beldashki.ru', '123', '2019-12-02 18:07:00', NULL, NULL, 3);

INSERT INTO public.task_statuses ("id", "name", "display_name")
VALUES
  (1, 'Draft', 'Черновик'),
  (2, 'New', 'Новая'),
  (3, 'InProgress', 'В процессе'),
  (4, 'Done', 'Выполнена'),
  (5, 'Redirected', 'Перенаправлена'),
  (6, 'Accepted', 'Принята'),
  (7, 'Rejected', 'Отклонена'),
  (8, 'Deleted', 'Удалена');


INSERT INTO public.incomingBox ("id", "year", "numb", "out_numb", "doc_date", "out_date", "title", "content", "user_created_id", "user_updated_id", "created_at", "updated_at", "deleted_at", "is_deleted")
VALUES
  (1, 2019, '101/11', '998/11/123', '2019-12-02', '2019-11-30', 'Комерческое предложение', 'Комерческое предложение на приобретение лицензий Kaspersky Antivirus (200 пользователей, 1 год)', 1, NULL, '2019-12-02 19:05:05', NULL, NULL, False),
  (2, 2019, '102/11', '566', '2019-12-02', '2019-11-26', 'Подтверждение оплаты', 'Подтверждение оплаты за блоки питания', 1, NULL, '2019-12-02 19:07:14', NULL, NULL, False),
  (3, 2019, '103/12', '799/К', '2019-12-02', '2019-11-27', 'Информация ИФНС', 'Информация по заполнению декларации по акцизам', 1, NULL, '2019-12-02 19:11:01', NULL, NULL, False);


INSERT INTO public.tasks (id, title, description, task_status_id, is_accepted, accept_time, user_created_id, user_updated_id, user_deleted_id, created_at, updated_at, deleted_at, is_deleted, task_user_id)
VALUES
  (1, 'Бизнес план январь 2020', 'Подготовить бизнесплано на январь 2020 по материалам, услугам', 1, False, NULL, 1, NULL, NULL, '2019-12-02 18:17:57', '2019-12-02 18:18:02', NULL, False, 1),
  (2, 'Отчет по материалам декабрь 2019', 'Подготовить отчет по использованным материалам за декабрь 2019', 2, False, NULL, 1, NULL, NULL, '2019-12-02 18:18:50', '2019-12-02 18:18:53', NULL, False, 1),
  (3, 'Отчет по закупкам декабрь 2019', 'Подготовить отчет по закупкам декабрь 2019', 3, False, NULL, 1, NULL, NULL, '2019-12-02 18:23:08', '2019-12-02 18:23:16', NULL, False, 3),
  (4, 'Тендер по закупке лицензий KAV', 'Подготовить техническое задание для проведения тендера на закупку KAV', 4, False, NULL, 1, 3, NULL, '2019-12-02 18:28:12', '2019-12-02 18:28:17', NULL, False, 3),
  (5, 'Сдать документы в бухгалтерию', 'Сдать акты выполненных работы по прокладке оптоволокна в бухгалтерию', 6, False, NULL, 1, 3, NULL, '2019-12-02 18:32:01', '2019-12-02 18:32:06', NULL, False, 3),
  (6, 'Выбор поставщика', 'По итогам проведенного тендара на закупку серверного оборудования подготовить документы о выборе поставщика', 7, False, NULL, 1, 3, NULL, '2019-12-02 18:35:07', '2019-12-02 18:35:15', NULL, False, 3),
  (7, 'Списание оборудования', 'Подготовить перечень оборудования для списания в январе 2020', 8, False, NULL, 1, 3, NULL, '2019-12-02 18:37:10', '2019-12-02 18:37:15', NULL, False, 3);
