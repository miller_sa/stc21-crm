
CREATE  TABLE roles (
	id                   serial  NOT NULL ,
	name                 varchar(255)  NOT NULL ,
	display_name         varchar(255)  NOT NULL ,
	description          varchar(255)   ,
	created_at           timestamp   ,
	updated_at           timestamp   ,
	CONSTRAINT pk_roles_id PRIMARY KEY ( id )
 );

COMMENT ON TABLE roles IS 'Роли пользователей';

COMMENT ON COLUMN roles.id IS 'Идентификатор';

COMMENT ON COLUMN roles.name IS 'Наименование';

COMMENT ON COLUMN roles.display_name IS 'Отображаемое наименование';

COMMENT ON COLUMN roles.description IS 'Описание';

COMMENT ON COLUMN roles.created_at IS 'Дата создания';

COMMENT ON COLUMN roles.updated_at IS 'Дата последнего изменения';

CREATE  TABLE task_statuses (
	id                   serial  NOT NULL ,
	name                 varchar(255)   ,
	display_name         varchar(255)   ,
	CONSTRAINT pk_task_statuses_id PRIMARY KEY ( id )
 );

COMMENT ON COLUMN task_statuses.id IS 'Идентификатор';

COMMENT ON COLUMN task_statuses.name IS 'Наименование статуса';

COMMENT ON COLUMN task_statuses.display_name IS 'Отображаемое наименование';

CREATE  TABLE users (
	id                   serial  NOT NULL ,
	login                varchar(100)  NOT NULL ,
	firstname            varchar(100)  NOT NULL ,
	lastname             varchar(100)  NOT NULL ,
	fathername           varchar(100)   ,
	email                varchar(255)   ,
	"password"           varchar(255)  NOT NULL ,
	created_at           timestamp   ,
	updated_at           timestamp   ,
	telegram_username varchar(32),
	CONSTRAINT pk_users_id PRIMARY KEY ( id )
 );

COMMENT ON TABLE users IS 'Пользователи';

COMMENT ON COLUMN users.id IS 'Идентификатор';

COMMENT ON COLUMN users.login IS 'Логин';

COMMENT ON COLUMN users.firstname IS 'Имя';

COMMENT ON COLUMN users.lastname IS 'Фамилия';

COMMENT ON COLUMN users.fathername IS 'Отчество';

COMMENT ON COLUMN users.email IS 'Эл. почта';

COMMENT ON COLUMN users."password" IS 'Пароль';

COMMENT ON COLUMN users.created_at IS 'Дата создания';

COMMENT ON COLUMN users.updated_at IS 'Дата изменения';

CREATE  TABLE incomingBox (
	id                   serial  NOT NULL ,
	"year"               integer   ,
	numb                 varchar(255)  NOT NULL ,
	out_numb             varchar(255)  NOT NULL ,
	doc_date             date  NOT NULL ,
	out_date             date  NOT NULL ,
	title                varchar(400)  NOT NULL ,
	content              text   ,
	user_created_id      integer   ,
	user_updated_id      integer   ,
	created_at           timestamp   ,
	updated_at           timestamp   ,
	deleted_at           timestamp   ,
	is_deleted           bool DEFAULT false NOT NULL ,
	CONSTRAINT pk_incomingBox_id PRIMARY KEY ( id )
 );

COMMENT ON TABLE incomingBox IS 'Входящая канцелярия';

COMMENT ON COLUMN incomingBox.id IS 'Идентификатор';

COMMENT ON COLUMN incomingBox."year" IS 'Год';

COMMENT ON COLUMN incomingBox.numb IS 'Номер входящего';

COMMENT ON COLUMN incomingBox.out_numb IS 'Исходящий номер';

COMMENT ON COLUMN incomingBox.doc_date IS 'Дата входящего';

COMMENT ON COLUMN incomingBox.out_date IS 'Дата исходящего';

COMMENT ON COLUMN incomingBox.title IS 'Заголовок';

COMMENT ON COLUMN incomingBox.content IS 'Содержание';

COMMENT ON COLUMN incomingBox.user_created_id IS 'Кем создано';

COMMENT ON COLUMN incomingBox.user_updated_id IS 'Кем обновлено';

COMMENT ON COLUMN incomingBox.created_at IS 'Дата создания';

COMMENT ON COLUMN incomingBox.updated_at IS 'Дата обновления';

COMMENT ON COLUMN incomingBox.deleted_at IS 'Дата "удаления"';

COMMENT ON COLUMN incomingBox.is_deleted IS 'Пометка на удаление';

CREATE  TABLE tasks (
	id                   serial  NOT NULL ,
	title                varchar(255)  NOT NULL ,
	description          text   ,
	task_status_id       integer  NOT NULL ,
	is_accepted          bool   ,
	accept_time          timestamp   ,
	user_created_id      integer   ,
	user_updated_id      integer   ,
	user_deleted_id      integer   ,
	created_at           timestamp   ,
	updated_at           timestamp   ,
	deleted_at           timestamp   ,
	is_deleted           bool   ,
	CONSTRAINT pk_tasks_id PRIMARY KEY ( id )
 );

COMMENT ON TABLE tasks IS 'Задачи';

COMMENT ON COLUMN tasks.id IS 'Идентификатор';

COMMENT ON COLUMN tasks.title IS 'Заголовок';

COMMENT ON COLUMN tasks.description IS 'Описание';

COMMENT ON COLUMN tasks.task_status_id IS 'Текуший статус';

COMMENT ON COLUMN tasks.is_accepted IS 'Принято/отклонено';

COMMENT ON COLUMN tasks.accept_time IS 'Дата/время принятия';

COMMENT ON COLUMN tasks.user_created_id IS 'Кем создано';

COMMENT ON COLUMN tasks.user_updated_id IS 'Кем обновлено';

COMMENT ON COLUMN tasks.user_deleted_id IS 'Кем удалено';

COMMENT ON COLUMN tasks.created_at IS 'Дата создания';

COMMENT ON COLUMN tasks.updated_at IS 'Дата обновления';

COMMENT ON COLUMN tasks.deleted_at IS 'Дата удаления';

COMMENT ON COLUMN tasks.is_deleted IS 'Пометка на удаление';

CREATE  TABLE user_roles (
	user_id              integer  NOT NULL ,
	role_id              integer  NOT NULL ,
	CONSTRAINT idx_user_roles PRIMARY KEY ( user_id, role_id )
 );

COMMENT ON TABLE user_roles IS 'Роли пользователей';

COMMENT ON COLUMN user_roles.user_id IS 'Пользователь id';

COMMENT ON COLUMN user_roles.role_id IS 'Роль id';

ALTER TABLE incomingBox ADD CONSTRAINT fk_incomingBox_created_user_id FOREIGN KEY ( user_created_id ) REFERENCES users( id ) ON DELETE RESTRICT;

ALTER TABLE incomingBox ADD CONSTRAINT fk_incomingBox_user_updated_id FOREIGN KEY ( user_updated_id ) REFERENCES users( id ) ON DELETE RESTRICT;

ALTER TABLE tasks ADD CONSTRAINT fk_tasks_task_statuses FOREIGN KEY ( task_status_id ) REFERENCES task_statuses( id ) ON DELETE RESTRICT;

ALTER TABLE tasks ADD CONSTRAINT fk_tasks_created_user_id FOREIGN KEY ( user_created_id ) REFERENCES users( id );

ALTER TABLE tasks ADD CONSTRAINT fk_tasks_updated_user_id FOREIGN KEY ( user_updated_id ) REFERENCES users( id ) ON DELETE RESTRICT;

ALTER TABLE tasks ADD CONSTRAINT fk_tasks_deleted_id FOREIGN KEY ( user_deleted_id ) REFERENCES users( id );

ALTER TABLE user_roles ADD CONSTRAINT fk_user_roles_user_id FOREIGN KEY ( user_id ) REFERENCES users( id );

ALTER TABLE user_roles ADD CONSTRAINT fk_user_roles_role_id FOREIGN KEY ( role_id ) REFERENCES roles( id );

ALTER TABLE users ADD role_id INTEGER;

ALTER TABLE public.users
  ADD CONSTRAINT users_fk_role_id FOREIGN KEY (role_id)
    REFERENCES public.roles(id)
    ON DELETE RESTRICT
    ON UPDATE NO ACTION
    NOT DEFERRABLE;

DROP TABLE IF EXISTS user_roles;

CREATE TABLE public.permissions (
  id SERIAL NOT NULL,
  name VARCHAR(255) NOT NULL UNIQUE,
  display_name VARCHAR(255) NOT NULL UNIQUE,
  created_at TIMESTAMP WITHOUT TIME ZONE,
  updated_at TIMESTAMP(0) WITHOUT TIME ZONE,
  PRIMARY KEY(id)
) ;

ALTER TABLE public.permissions
  ALTER COLUMN id SET STATISTICS 0;

ALTER TABLE public.permissions
  ALTER COLUMN name SET STATISTICS 0;

ALTER TABLE public.permissions
  ALTER COLUMN display_name SET STATISTICS 0;

COMMENT ON COLUMN public.permissions.id
IS 'Идентификатор';

COMMENT ON COLUMN public.permissions.name
IS 'Имя';

COMMENT ON COLUMN public.permissions.display_name
IS 'Отображаемое имя';

COMMENT ON COLUMN public.permissions.created_at
IS 'Дата создания';

COMMENT ON COLUMN public.permissions.updated_at
IS 'Дата последнего обновления';

CREATE TABLE public.role_permissions (
  role_id INTEGER NOT NULL,
  permission_id INTEGER NOT NULL
) ;

ALTER TABLE public.role_permissions
  ADD CONSTRAINT role_permissions_idx 
    PRIMARY KEY (role_id, permission_id) NOT DEFERRABLE;
    
ALTER TABLE public.role_permissions
  ADD CONSTRAINT role_permissions_fk_role_id FOREIGN KEY (role_id)
    REFERENCES public.roles(id)
    ON DELETE RESTRICT
    ON UPDATE NO ACTION
    NOT DEFERRABLE;

ALTER TABLE public.role_permissions
  ADD CONSTRAINT role_permissions_fk_permission_id FOREIGN KEY (permission_id)
    REFERENCES public.permissions(id)
    ON DELETE RESTRICT
    ON UPDATE NO ACTION
    NOT DEFERRABLE;

ALTER TABLE public.tasks
  ADD COLUMN task_user_id INTEGER;

ALTER TABLE public.tasks
  ADD CONSTRAINT tasks_fk_task_user_id FOREIGN KEY (task_user_id)
    REFERENCES public.users(id)
    ON DELETE RESTRICT
    ON UPDATE NO ACTION
    NOT DEFERRABLE;
    
ALTER TABLE public.task_statuses
    ADD CONSTRAINT task_statuses_idx_uk_name
        UNIQUE (name) NOT DEFERRABLE;
        
ALTER TABLE public.users
    ADD CONSTRAINT users_idx_uk_login
        UNIQUE (login) NOT DEFERRABLE;