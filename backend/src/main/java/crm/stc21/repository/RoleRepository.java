package crm.stc21.repository;

import crm.stc21.entity.RoleEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface RoleRepository extends CrudRepository<RoleEntity, Long> {

    Collection<RoleEntity> findByNameNotNull();
    RoleEntity findRoleEntityById(Long id);

}
