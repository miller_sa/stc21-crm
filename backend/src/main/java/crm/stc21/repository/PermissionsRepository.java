package crm.stc21.repository;

import crm.stc21.entity.PermissionEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PermissionsRepository extends CrudRepository<PermissionEntity, Long> {
    PermissionEntity findByName(String name);
    List<PermissionEntity> findByNameNotNull();
}
