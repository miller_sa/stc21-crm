package crm.stc21.repository;

import crm.stc21.entity.OutBoxEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface OutBoxRepository extends CrudRepository<OutBoxEntity, Long> {
    Collection<OutBoxEntity> findByIsDeletedFalse();
}
