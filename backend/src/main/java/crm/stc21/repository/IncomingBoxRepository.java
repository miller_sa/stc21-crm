package crm.stc21.repository;

import crm.stc21.entity.IncomingBoxEntity;
import crm.stc21.entity.InnerboxTypeNameEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Optional;

@Repository
public interface IncomingBoxRepository extends CrudRepository<IncomingBoxEntity, Long> {
    Collection<IncomingBoxEntity> findByIsDeletedFalse();
    @Query("SELECT MAX(u.number) FROM IncomingBoxEntity u WHERE u.isDeleted = false AND u.typeDocId = ?1")
    Optional<Long> findByNumberMaxFromTypeId(InnerboxTypeNameEntity typeId);

}
