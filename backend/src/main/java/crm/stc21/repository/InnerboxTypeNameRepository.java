package crm.stc21.repository;

import crm.stc21.entity.InnerboxTypeNameEntity;
import crm.stc21.entity.TaskStatusEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface InnerboxTypeNameRepository extends CrudRepository<InnerboxTypeNameEntity, Long> {
    Optional<InnerboxTypeNameEntity> findByName(String Name);
    List<InnerboxTypeNameEntity> findAll();

}
