package crm.stc21.repository;

import crm.stc21.entity.TaskStatusEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface TaskStatusRepository extends CrudRepository<TaskStatusEntity, Long> {
    Optional<TaskStatusEntity> findByName(String Name);
}
