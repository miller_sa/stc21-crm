package crm.stc21.repository;

import crm.stc21.entity.TaskEntity;
import crm.stc21.entity.TaskStatusEntity;
import crm.stc21.entity.UserEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface TaskRepository extends CrudRepository<TaskEntity, Long> {
    Collection<TaskEntity> findByTaskStatusNot(TaskStatusEntity taskStatusEntity);
    Collection<TaskEntity> findByUserAndTaskStatusNotIn(UserEntity user, Collection<TaskStatusEntity> taskStatuses);
    Long countByTaskStatus(TaskStatusEntity taskStatus);
}
