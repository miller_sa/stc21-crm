package crm.stc21.repository;

import crm.stc21.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<UserEntity, Long> {
    UserEntity findByLogin(String login);
    List<UserEntity> findAllByIsActive(Boolean isActive);
    @Query(value = "SELECT U.*  FROM Users U  LEFT JOIN tasks T ON (T.task_user_id = U.id AND T.inner_id = ?1)  WHERE (T.id IS NULL)",
            nativeQuery = true)
    List<UserEntity> findUsersWhereNotTaskInner(Long id);
    @Query(value = "SELECT U.* FROM Tasks T LEFT JOIN Users U ON (U.id = T.task_user_id) WHERE (T.inner_id = ?1)",
            nativeQuery = true)
    List<UserEntity> findUsersWhereTaskInner(Long id);
    @Query(value = "SELECT U.*  FROM Users U  LEFT JOIN tasks T ON (T.task_user_id = U.id AND T.incoming_id = ?1)  WHERE (T.id IS NULL)",
            nativeQuery = true)
    List<UserEntity> findUsersWhereNotTaskIncoming(Long id);
    @Query(value = "SELECT U.* FROM Tasks T LEFT JOIN Users U ON (U.id = T.task_user_id) WHERE (T.incoming_id = ?1)",
            nativeQuery = true)
    List<UserEntity> findUsersWhereTaskIncoming(Long id);

}
