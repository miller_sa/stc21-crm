package crm.stc21.service;

import crm.stc21.entity.*;
import crm.stc21.reference.TaskPermissionsRef;
import crm.stc21.reference.TaskStatusRef;
import crm.stc21.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TaskService implements ITaskService {
    final String ERROR_403_VIEW = "errors/403";

    private TaskRepository taskRepository;
    private UserService userService;
    private TaskStatusRef taskStatusRef;
    private TaskPermissionsRef taskPermissionsRef;
    private InnerboxService innerboxService;
    private IncomingBoxService incomingBoxService;


    @Override
    public List<TaskEntity> findAllTasks() {
        if (userService.isAuthUserHasPermission(taskPermissionsRef.getTaskListAllPermission())) {
            return (List<TaskEntity>) taskRepository
                    .findByTaskStatusNot(taskStatusRef.getDeletedStatus());
        } else {

            List<TaskStatusEntity> taskStatusesNotShow = new ArrayList<>();
            taskStatusesNotShow.add(taskStatusRef.getDeletedStatus());
            taskStatusesNotShow.add(taskStatusRef.getDraftTaskStatus());
            return (List<TaskEntity>) taskRepository
                    .findByUserAndTaskStatusNotIn(userService.getAuthUser(),
                            taskStatusesNotShow);
        }
    }

    @Override
    public Long saveTask(TaskEntity taskEntity) {
        if (taskEntity.getId() == null) {
//            TaskStatusEntity taskStatusEntity = taskStatusRef.getDraftTaskStatus();
//            taskEntity.setTaskStatus(taskStatusEntity);
            taskEntity.setAccepted(false);
            UserEntity userEntity = userService.getAuthUser();
            taskEntity.setUserCreated(userEntity);
            taskEntity.setCreatedAt(new Timestamp(System.currentTimeMillis()));
            taskEntity.setDeleted(false);
//            taskEntity.setUser(userEntity);
            taskRepository.save(taskEntity);
            return taskRepository.save(taskEntity).getId();
        } else {
            TaskEntity newTaskEntity = findById(taskEntity.getId());
            newTaskEntity.setId(taskEntity.getId());
            newTaskEntity.setTitle(taskEntity.getTitle());
            newTaskEntity.setDescription(taskEntity.getDescription());
            newTaskEntity.setUpdatedAt(new Timestamp(System.currentTimeMillis()));
            newTaskEntity.setTaskStatus(taskEntity.getTaskStatus());
            UserEntity userEntity = userService.getAuthUser();
            newTaskEntity.setUserUpdated(userEntity);
            newTaskEntity.setUser(taskEntity.getUser());
            return taskRepository.save(newTaskEntity).getId();
        }
    }

    @Override
    public void saveInnerTask(Long userId, Long innerId, boolean update) {
        InnerboxEntity innerData = innerboxService.findInnerById(innerId);
        if (innerData.getId() != null && update == false) {
            UserEntity userEntity = userService.getAuthUser();
            TaskEntity taskEntity = new TaskEntity();
            taskEntity.setTitle(innerData.getTitle());
            taskEntity.setDeleted(false);
            taskEntity.setAccepted(false);
            UserEntity userTask = userService.findById(userId);
            taskEntity.setUser(userTask);
            taskEntity.setUserCreated(userEntity);
            taskEntity.setTaskInner(innerData);
            taskEntity.setDeadLine(innerData.getDeadLine());
            taskEntity.setCreatedAt(new Timestamp(System.currentTimeMillis()));
            TaskStatusEntity taskStatusEntity = taskStatusRef.getDraftTaskStatus();
            taskEntity.setTaskStatus(taskStatusEntity);
            taskRepository.save(taskEntity);
        } /*else {

        }*/
    }

    @Override
    public void saveIncomingTask(Long userId, Long incomingId, boolean update) {
        IncomingBoxEntity incomingData = incomingBoxService.findIncomingBoxById(incomingId);

        if (incomingData.getId() != null && update == false) {
            UserEntity userEntity = userService.getAuthUser();
            TaskEntity taskEntity = new TaskEntity();
            taskEntity.setTitle(incomingData.getTitle());
            taskEntity.setDeleted(false);
            taskEntity.setAccepted(false);
            UserEntity userTask = userService.findById(userId);
            taskEntity.setUser(userTask);
            taskEntity.setUserCreated(userEntity);
            taskEntity.setTaskIncom(incomingData);
            taskEntity.setDeadLine(incomingData.getDeadLine());
            taskEntity.setCreatedAt(new Timestamp(System.currentTimeMillis()));
            TaskStatusEntity taskStatusEntity = taskStatusRef.getDraftTaskStatus();
            taskEntity.setTaskStatus(taskStatusEntity);
            taskRepository.save(taskEntity);
        } /*else {

        }*/
    }

    @Override
    public void deleteTask(Long id) {
        Optional<TaskEntity> taskEntity = taskRepository.findById(id);
        if (taskEntity.isPresent()) {
            taskEntity.get().setDeleted(true);
            TaskStatusEntity taskStatusEntity = taskStatusRef.getDeletedStatus();
            taskEntity.get().setTaskStatus(taskStatusEntity);
            taskEntity.get().setUserDeleted(userService.getAuthUser());
            taskEntity.get().setDeletedAt(new Timestamp(System.currentTimeMillis()));
            taskRepository.save(taskEntity.get());
        }
    }

    @Override
    public TaskEntity findById(Long id) {
        TaskEntity taskEntity = taskRepository.findById(id).orElse(new TaskEntity());
        return taskEntity;
    }

    @Override
    public boolean isCanAuthUserAcceptRejectThisTask(TaskEntity taskEntity) {
        if (userService.isAuthUserHasPermission(taskPermissionsRef.getTaskProcessPermission())) {
            if (taskEntity.getUser().getId().equals(userService.getAuthUser().getId())
                    && taskEntity.getTaskStatus().equals(taskStatusRef.getAssignedTaskStatus())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean isCanAuthUserDoneThisTask(TaskEntity taskEntity) {
        if (userService.isAuthUserHasPermission(taskPermissionsRef.getTaskProcessPermission())) {
            if (taskEntity.getUser().getId().equals(userService.getAuthUser().getId())
                    && taskEntity.getTaskStatus().equals(taskStatusRef.getInWorkStatus())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean isCanAuthUserAssignedThisTask(TaskEntity taskEntity) {
        if (userService.isAuthUserHasPermission(taskPermissionsRef.getTaskProcessPermission())) {
            if (taskStatusRef.getDraftTaskStatus().equals(taskEntity.getTaskStatus())) {
                return true;
            }
        }
        return false;
    }


    @Override
    public void acceptTask(Long id) {
        TaskEntity taskEntity = taskRepository.findById(id).get();
        if (isCanAuthUserAcceptRejectThisTask(taskEntity)) {
            taskEntity.setTaskStatus(taskStatusRef.getInWorkStatus());
            taskEntity.setAccepted(true);
            taskEntity.setUserUpdated(userService.getAuthUser());
            taskEntity.setUpdatedAt(new Timestamp(System.currentTimeMillis()));
            taskRepository.save(taskEntity);
        }
    }

    @Override
    public void rejectTask(Long id) {
        TaskEntity taskEntity = taskRepository.findById(id).get();
        if (isCanAuthUserAcceptRejectThisTask(taskEntity)) {
            taskEntity.setTaskStatus(taskStatusRef.getRejectedStatus());
            taskEntity.setAccepted(false);
            taskEntity.setUserUpdated(userService.getAuthUser());
            taskEntity.setUpdatedAt(new Timestamp(System.currentTimeMillis()));
            taskRepository.save(taskEntity);
        }
    }

    @Override
    public void doneTask(Long id) {
        TaskEntity taskEntity = taskRepository.findById(id).get();
        if (isCanAuthUserDoneThisTask(taskEntity)) {
            taskEntity.setTaskStatus(taskStatusRef.getDoneStatus());
            taskEntity.setUpdatedAt(new Timestamp(System.currentTimeMillis()));
            taskEntity.setUserUpdated(userService.getAuthUser());
            taskRepository.save(taskEntity);
        }
    }


    @Autowired
    public TaskService(InnerboxService innerboxService, IncomingBoxService incomingBoxService) {
        this.innerboxService = innerboxService;
        this.incomingBoxService = incomingBoxService;
    }



    @Autowired
    public void setTaskRepository(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setTaskStatusRef(TaskStatusRef taskStatusRef) {
        this.taskStatusRef = taskStatusRef;
    }

    @Autowired
    public void setTaskPermissionsRef(TaskPermissionsRef taskPermissionsRef) {
        this.taskPermissionsRef = taskPermissionsRef;
    }
}
