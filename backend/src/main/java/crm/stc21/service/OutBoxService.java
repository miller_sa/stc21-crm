package crm.stc21.service;

import crm.stc21.entity.OutBoxEntity;
import crm.stc21.entity.UserEntity;
import crm.stc21.repository.OutBoxRepository;
import crm.stc21.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;


@Service
public class OutBoxService implements IOutBoxService {

    OutBoxRepository outBoxRepository;
    UserRepository userRepository;
    UserService userService;


    @Override
    public List<OutBoxEntity> findAllOutBox() {
        return (List<OutBoxEntity>) outBoxRepository.findByIsDeletedFalse();
    }

    @Override
    public Long saveOutBox(OutBoxEntity outBoxEntity) {
        if (outBoxEntity.getId() == null) {
            UserEntity userEntity = userService.getAuthUser();
            outBoxEntity.setUserCreatedId(userEntity);
            outBoxEntity.setCreatedAt(new Timestamp(System.currentTimeMillis()));
            outBoxEntity.setDeleted(false);
            return outBoxRepository.save(outBoxEntity).getId();
        } else {
            OutBoxEntity outBoxEntityOther = findbyIdOutBox(outBoxEntity.getId());
            outBoxEntityOther.setYear(outBoxEntity.getYear());
            outBoxEntityOther.setNumber(outBoxEntity.getNumber());
            outBoxEntityOther.setInboxNumber(outBoxEntity.getInboxNumber());
            outBoxEntityOther.setInboxDate(outBoxEntity.getInboxDate());
            outBoxEntityOther.setDocDate(outBoxEntity.getDocDate());
            outBoxEntityOther.setTitle(outBoxEntity.getTitle());
            outBoxEntityOther.setContent(outBoxEntity.getContent());
            outBoxEntityOther.setUserUpdatedId(outBoxEntity.getUserUpdatedId());
            outBoxEntityOther.setUpdatedAt(new Timestamp(System.currentTimeMillis()));
            outBoxEntityOther.setTypeOfOutbox(outBoxEntity.getTypeOfOutbox());
            return outBoxRepository.save(outBoxEntityOther).getId();
        }
    }

    public static Long getCurrentYear() {
        java.util.Calendar calendar = java.util.Calendar.getInstance(java.util.TimeZone.getDefault(), java.util.Locale.getDefault());
        calendar.setTime(new java.util.Date());
        long k;
        if (Calendar.MONTH == 0){
            return (long) calendar.get(java.util.Calendar.YEAR)-1;
        }else {
            return (long) calendar.get(java.util.Calendar.YEAR);
        }
    }

    @Override
    public OutBoxEntity findbyIdOutBox(Long id) {
        OutBoxEntity outBoxEntity = outBoxRepository.findById(id).orElse(new OutBoxEntity());
        return outBoxEntity;
    }

    @Override
    public void deleteOutBox(Long id) {
        Optional<OutBoxEntity> outBoxEntity = outBoxRepository.findById(id);
        if (outBoxEntity.isPresent()) {
            outBoxEntity.get().setDeleted(true);
            outBoxEntity.get().setDeletedAt(new Timestamp((System.currentTimeMillis())));
            outBoxRepository.save(outBoxEntity.get());
        }
    }

    @Autowired
    public void setOutBoxRepository(OutBoxRepository outBoxRepository) {
        this.outBoxRepository = outBoxRepository;
    }

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}
