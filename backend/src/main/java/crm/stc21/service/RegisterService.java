package crm.stc21.service;

import crm.stc21.entity.UserEntity;
import org.springframework.stereotype.Service;

import java.util.List;

public interface RegisterService {
    void saveUser(UserEntity user);
    String validation(UserEntity user);
    List<UserEntity> allUsers();
    List<UserEntity> usersNotIn(Long id);
    List<UserEntity> usersIn(Long id);
    List<UserEntity> usersIncomNotIn(Long id);
    List<UserEntity> usersIncomIn(Long id);

}
