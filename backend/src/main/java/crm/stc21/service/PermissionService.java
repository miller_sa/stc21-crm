package crm.stc21.service;

import crm.stc21.entity.PermissionEntity;
import crm.stc21.repository.PermissionsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PermissionService implements IPermissionService {

    private PermissionsRepository permissionsRepository;

    @Autowired
    public void setPermissionsRepository(PermissionsRepository permissionsRepository) {
        this.permissionsRepository = permissionsRepository;
    }

    @Override
    public List<PermissionEntity> getPermissions() {
        return (List<PermissionEntity>) permissionsRepository.findAll();
    }
}
