package crm.stc21.service;

import crm.stc21.entity.InnerboxEntity;
import crm.stc21.entity.InnerboxTypeNameEntity;
import crm.stc21.repository.InnerboxTypeNameRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InnerboxTypeNameService implements IInnerboxTypeNameService {
    InnerboxTypeNameRepository innerboxTypeNameRepository;

    public InnerboxTypeNameService(InnerboxTypeNameRepository innerboxTypeNameRepository) {
        this.innerboxTypeNameRepository = innerboxTypeNameRepository;
    }

    @Override
    public List<InnerboxTypeNameEntity> findAllTypes() {
        return innerboxTypeNameRepository.findAll();
    }
}
