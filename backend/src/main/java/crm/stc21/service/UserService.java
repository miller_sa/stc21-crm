package crm.stc21.service;

import crm.stc21.entity.InnerboxEntity;
import crm.stc21.entity.PermissionEntity;
import crm.stc21.entity.UserEntity;
import crm.stc21.repository.RoleRepository;
import crm.stc21.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class UserService implements UserDetailsService {
    private UserRepository userRepository;
    private RoleRepository roleRepository;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        UserEntity userEntity = userRepository.findByLogin(s);
        if (userEntity == null) {
            throw new UsernameNotFoundException("Пользователь не найден !");
        } else {
            return userEntity;
        }
    }

    public UserEntity findUserById(Long id) {
        UserEntity userEntity = userRepository.findById(id).get();
        return userEntity;
    }

    public UserEntity getAuthUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserEntity userEntity = (UserEntity) authentication.getPrincipal();
        return userEntity;
    }

    public List<UserEntity> getAllActiveUsers(){
        return userRepository.findAllByIsActive(true);
    };

    public boolean isAuthUserHasPermission(PermissionEntity permissionEntity) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Set<String> roles = AuthorityUtils.authorityListToSet(authentication.getAuthorities());
        return roles.contains(permissionEntity.getName());
    }

    public UserEntity findById(Long id) {
        UserEntity userEntity = userRepository.findById(id).orElse(new UserEntity());
        return userEntity;
    }
    public void saveUser(UserEntity userEntity) {
        if (userEntity.getId() == null) {
            userEntity.setActive(true);
            userRepository.save(userEntity);
        } else {
            UserEntity newUserEntity = findUserById(userEntity.getId());
            newUserEntity.setId(userEntity.getId());
            newUserEntity.setLogin(userEntity.getUsername());
            newUserEntity.setFirstName(userEntity.getFirstName());
            newUserEntity.setLastName(userEntity.getLastName());
            newUserEntity.setFatherName(userEntity.getFatherName());
            newUserEntity.setEmail(userEntity.getEmail());
            newUserEntity.setPassword(userEntity.getPassword() != "" ? userEntity.getPassword() : newUserEntity.getPassword());
            newUserEntity.setCreatedAt(userEntity.getCreatedAt());
            newUserEntity.setUpdatedAt(new Timestamp(System.currentTimeMillis()));
            newUserEntity.setTelegramUsername(userEntity.getTelegramUsername());
            newUserEntity.setRole(userEntity.getRole());

            System.out.println(newUserEntity.getActive());
            userRepository.save(newUserEntity);
        }
    }

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setRoleRepository(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }
}
