package crm.stc21.service;

import crm.stc21.entity.PermissionEntity;
import crm.stc21.entity.RoleEntity;
import crm.stc21.entity.UserEntity;
import crm.stc21.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.management.relation.Role;
import java.sql.Timestamp;
import java.util.List;

@Service
public class RoleService implements IRoleService {

    private RoleRepository roleRepository;

    @Autowired
    public void setRoleRepository(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public List<RoleEntity> getAllRoles() {
        return (List<RoleEntity>) roleRepository.findByNameNotNull();
    }

    @Override
    public RoleEntity getRoleById(Long id) {
        return roleRepository.findRoleEntityById(id);
    }

    @Override
    public void saveRole(RoleEntity roleEntity) {
        if (roleEntity.getId() == null) {
            roleEntity.setCreatedAt(new Timestamp(System.currentTimeMillis()));
            roleRepository.save(roleEntity);
        } else {
            RoleEntity newRoleEntity = (roleRepository.findRoleEntityById(roleEntity.getId()));
            newRoleEntity.setId(roleEntity.getId());
            newRoleEntity.setName(roleEntity.getName());
            newRoleEntity.setUpdatedAt(new Timestamp(System.currentTimeMillis()));
            newRoleEntity.setPermissions(roleEntity.getPermissions());
            roleRepository.save(newRoleEntity);
        }
    }

}
