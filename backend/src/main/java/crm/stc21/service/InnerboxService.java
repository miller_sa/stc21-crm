package crm.stc21.service;

import crm.stc21.entity.InnerboxEntity;
import crm.stc21.entity.UserEntity;
import crm.stc21.reference.TaskStatusRef;
import crm.stc21.repository.InnerboxRepository;
import crm.stc21.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Optional;

@Service
public class InnerboxService implements IInnerboxService {
    InnerboxRepository innerboxRepository;

    UserService userService;
    TaskService taskService;
    @Autowired
    TaskStatusRef taskStatusRef;

    private TaskRepository taskRepository;

    @Override
    public List<InnerboxEntity> findAllInner() {
        return (List<InnerboxEntity>) innerboxRepository.findByIsDeletedFalse();
    }

    @Override
    public Long saveInner(InnerboxEntity innerboxEntity) {
        if (innerboxEntity.getId() == null) {
            UserEntity userEntity = userService.getAuthUser();
            innerboxEntity.setYear(getNowYear());
            if (innerboxRepository.findByNumberMaxFromTypeId(innerboxEntity.getTypeDocId()).isPresent()) {
                innerboxEntity.setNumber(innerboxRepository.findByNumberMaxFromTypeId(innerboxEntity.getTypeDocId()).get() + 1L);
            } else {
                innerboxEntity.setNumber(1L);
            }
            innerboxEntity.setDataAdoption(innerboxEntity.getDataAdoption());
            innerboxEntity.setTasksInner(innerboxEntity.getTasksInner());
            innerboxEntity.setUserCreatedId(userEntity);
            innerboxEntity.setDeleted(false);
            innerboxEntity.setAuthorId(innerboxEntity.getAuthorId());
            innerboxEntity.setDeadLine(innerboxEntity.getDeadLine());
            innerboxEntity.setTitle(innerboxEntity.getTitle());
            innerboxEntity.setContent(innerboxEntity.getContent());
            innerboxEntity.setTypeDocId(innerboxEntity.getTypeDocId());
            innerboxEntity.setCreatedAt(new Timestamp(System.currentTimeMillis()));
            return innerboxRepository.save(innerboxEntity).getId();
        } else {
            InnerboxEntity newInner = findInnerById(innerboxEntity.getId());
            // newInner.setYear(innerboxEntity.getYear());
            // newInner.setNumber(innerboxEntity.getNumber());
            newInner.setTitle(innerboxEntity.getTitle());
            newInner.setAuthorId(innerboxEntity.getAuthorId());
            newInner.setDataAdoption(innerboxEntity.getDataAdoption());
            newInner.setDeadLine(innerboxEntity.getDeadLine());
            newInner.setContent(innerboxEntity.getContent());
          //  newInner.setTypeDocId(innerboxEntity.getTypeDocId());
            newInner.setTasksInner(innerboxEntity.getTasksInner());
            newInner.setTitle(innerboxEntity.getTitle());
            newInner.setUpdatedAt(new Timestamp(System.currentTimeMillis()));
            UserEntity userEntity = userService.getAuthUser();
            newInner.setUserUpdatedId(userEntity);
            return innerboxRepository.save(newInner).getId();
        }
    }


    @Override
    public InnerboxEntity findInnerById(Long id) {
        InnerboxEntity innerboxEntity = innerboxRepository.findById(id).orElse(new InnerboxEntity());
        return innerboxEntity;
    }

    @Override
    public void deleteInner(Long id) {
        Optional<InnerboxEntity> innerboxEntity = innerboxRepository.findById(id);
        if (innerboxEntity.isPresent()) {
            innerboxEntity.get().setDeleted(true);
            innerboxEntity.get().setUpdatedAt(new Timestamp(System.currentTimeMillis()));
            innerboxRepository.save(innerboxEntity.get());
        }
    }

    private Long getNowYear() {
        Calendar calendar = new GregorianCalendar();
        return (long) calendar.get(Calendar.YEAR);
    }

    @Autowired
    public InnerboxService(InnerboxRepository innerboxRepository, UserService userService) {
        this.innerboxRepository = innerboxRepository;
        this.userService = userService;
    }
}
