package crm.stc21.service;

import crm.stc21.entity.TaskEntity;
import crm.stc21.entity.UserEntity;
import crm.stc21.entity.TaskStatusEntity;

import java.util.List;

public interface ITaskService {
    List<TaskEntity> findAllTasks();
    Long saveTask(TaskEntity taskEntity);
    void saveInnerTask(Long userId, Long innerId, boolean update);
    void saveIncomingTask(Long userId, Long incomingId, boolean update);
    TaskEntity findById(Long id);
    void deleteTask(Long id);
    void doneTask(Long id);
    void acceptTask(Long id);
    void rejectTask(Long id);
    boolean isCanAuthUserAcceptRejectThisTask(TaskEntity taskEntity);
    boolean isCanAuthUserDoneThisTask(TaskEntity taskEntity);
    boolean isCanAuthUserAssignedThisTask(TaskEntity taskEntity);
}
