package crm.stc21.service;

import crm.stc21.entity.InnerboxEntity;
import crm.stc21.entity.TaskEntity;

import java.util.List;

public interface IInnerboxService {
    List<InnerboxEntity> findAllInner();
    Long saveInner(InnerboxEntity innerboxEntity);
    InnerboxEntity findInnerById(Long id);
    void deleteInner(Long id);
  //  void saveInnerTasks(Long executor, Long inner);
}
