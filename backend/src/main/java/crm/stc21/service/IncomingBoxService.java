package crm.stc21.service;

import crm.stc21.entity.IncomingBoxEntity;
import crm.stc21.entity.UserEntity;
import crm.stc21.repository.IncomingBoxRepository;
import crm.stc21.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Optional;

@Service
public class IncomingBoxService implements IIncomingBoxService {

    private IncomingBoxRepository incomingBoxRepository;
    private UserRepository userRepository;
    private UserService userService;

    @Override
    public List<IncomingBoxEntity> findAllIncomingBox() {
        return (List<IncomingBoxEntity>) incomingBoxRepository.findByIsDeletedFalse();
    }

    @Override
    public Long saveIncomingBox(IncomingBoxEntity incomingBoxEntity) {
        if (incomingBoxEntity.getId() == null) {
            UserEntity userEntity = userService.getAuthUser();
            incomingBoxEntity.setYear(getNowYear());
            if (incomingBoxRepository.findByNumberMaxFromTypeId(incomingBoxEntity.getTypeDocId()).isPresent()) {
                incomingBoxEntity.setNumber(incomingBoxRepository.findByNumberMaxFromTypeId(incomingBoxEntity.getTypeDocId()).get() + 1L);
            } else {
                incomingBoxEntity.setNumber(1L);
            }
            incomingBoxEntity.setUserCreated(userEntity);
            incomingBoxEntity.setTaskIncoming(incomingBoxEntity.getTaskIncoming());
            incomingBoxEntity.setOutDate(incomingBoxEntity.getOutDate());
            incomingBoxEntity.setOutNumb(incomingBoxEntity.getOutNumb());
            incomingBoxEntity.setDocDate(incomingBoxEntity.getDocDate());
            incomingBoxEntity.setCreatedAt(new Timestamp(System.currentTimeMillis()));
            incomingBoxEntity.setDeleted(false);
            incomingBoxEntity.setTypeDocId(incomingBoxEntity.getTypeDocId());
            incomingBoxEntity.setDeadLine(incomingBoxEntity.getDeadLine());
            return incomingBoxRepository.save(incomingBoxEntity).getId();
        } else {
            IncomingBoxEntity newIncomingBoxEntity = findIncomingBoxById(incomingBoxEntity.getId());
            //newIncomingBoxEntity.setYear(incomingBoxEntity.getYear());
            newIncomingBoxEntity.setDocDate(incomingBoxEntity.getDocDate());
            newIncomingBoxEntity.setOutDate(incomingBoxEntity.getOutDate());
            // newIncomingBoxEntity.setNumber(incomingBoxEntity.getNumber());
            newIncomingBoxEntity.setOutNumb(incomingBoxEntity.getOutNumb());
            newIncomingBoxEntity.setTitle(incomingBoxEntity.getTitle());
            newIncomingBoxEntity.setContent(incomingBoxEntity.getContent());
            newIncomingBoxEntity.setDeadLine(incomingBoxEntity.getDeadLine());
            newIncomingBoxEntity.setUserUpdated(userService.getAuthUser());
            newIncomingBoxEntity.setTaskIncoming(incomingBoxEntity.getTaskIncoming());
            newIncomingBoxEntity.setUpdatedAt(new Timestamp(System.currentTimeMillis()));
            return incomingBoxRepository.save(newIncomingBoxEntity).getId();
        }
    }

    @Override
    public IncomingBoxEntity findIncomingBoxById(Long id) {
        IncomingBoxEntity inboxEntity = incomingBoxRepository.findById(id).orElse(new IncomingBoxEntity());
        return inboxEntity;
    }

    @Override
    public void deleteIncomingBox(Long id) {
        Optional<IncomingBoxEntity> incomingBoxEntity = incomingBoxRepository.findById(id);
        if (incomingBoxEntity.isPresent()) {
            incomingBoxEntity.get().setDeleted(true);
            incomingBoxEntity.get().setDeletedAt(new Timestamp(System.currentTimeMillis()));
            incomingBoxRepository.save(incomingBoxEntity.get());
        }
    }

    private Long getNowYear() {
        Calendar calendar = new GregorianCalendar();
        return (long) calendar.get(Calendar.YEAR);
    }

    @Autowired
    public void setIncomingBoxRepository(IncomingBoxRepository incomingBoxRepository) {
        this.incomingBoxRepository = incomingBoxRepository;
    }

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

}
