package crm.stc21.service;

import crm.stc21.entity.PermissionEntity;

import java.util.List;

public interface IPermissionService {

    List<PermissionEntity> getPermissions();

}
