package crm.stc21.service;

import crm.stc21.entity.PermissionEntity;
import crm.stc21.entity.RoleEntity;
import crm.stc21.entity.UserEntity;

import java.util.List;

public interface IRoleService {
    List <RoleEntity> getAllRoles();
    RoleEntity getRoleById(Long id);
    void saveRole(RoleEntity roleEntity);
}
