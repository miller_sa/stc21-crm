package crm.stc21.service;

import crm.stc21.entity.IncomingBoxEntity;

import java.util.List;

public interface IIncomingBoxService {
    List<IncomingBoxEntity> findAllIncomingBox();
    Long saveIncomingBox(IncomingBoxEntity taskEntity);
    IncomingBoxEntity findIncomingBoxById(Long id);
    void deleteIncomingBox(Long id);
}
