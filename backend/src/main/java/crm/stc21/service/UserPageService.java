package crm.stc21.service;

import crm.stc21.entity.UserEntity;
import crm.stc21.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;

@Service
public class UserPageService {
    UserRepository userRepository;
    UserService userService;

    public UserEntity getUser() {
        return userService.getAuthUser();
    }

    public void updateUser(UserEntity updatedUser, UserEntity currentUser) {
        currentUser.setUpdatedAt(new Timestamp(System.currentTimeMillis()));
        currentUser.setLogin(updatedUser.getLogin()!= null ? updatedUser.getLogin() : currentUser.getLogin());
        currentUser.setFirstName(updatedUser.getFirstName()!= null ? updatedUser.getFirstName() : currentUser.getFirstName());
        currentUser.setLastName(updatedUser.getLastName()!= null ? updatedUser.getLastName() : currentUser.getLastName());
        currentUser.setFatherName(updatedUser.getFatherName()!= null ? updatedUser.getFatherName() : currentUser.getFatherName());
        currentUser.setTelegramUsername(updatedUser.getTelegramUsername() != null ? updatedUser.getTelegramUsername() : currentUser.getTelegramUsername());
        currentUser.setPassword(updatedUser.getPassword() != "" ? updatedUser.getPassword() : currentUser.getPassword());
        userRepository.save(currentUser);
    }

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}
