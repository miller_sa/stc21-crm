package crm.stc21.service;

import crm.stc21.entity.UserEntity;
import crm.stc21.repository.RoleRepository;
import crm.stc21.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.List;

@Service
public class RegisterServiceImpl implements RegisterService {
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private UserRepository userRepository;


    @Override
    public String validation(UserEntity user) {
        if (!user.getPassword().equals(user.getConfirmPassword())) return "password";
        if (userRepository.findByLogin(user.getLogin()) != null) return "sameUser";
        if (user.getLogin().isEmpty() || user.getPassword().isEmpty()
                || user.getEmail().isEmpty() || user.getConfirmPassword().isEmpty()
                || user.getFirstName().isEmpty() || user.getLastName().isEmpty()
                || user.getFatherName().isEmpty()) return "sameField";
        if (!user.getEmail().contains("@")) return "email";
        if (user.getLogin().length() < 3 || user.getLogin().length() > 32) return "user";
        if (user.getPassword().length() > 32) return "longPassword";
        return "true";
    }

    @Override
    public void saveUser(UserEntity user) {
        user.setCreatedAt(LocalDate.now());
        user.setRole(roleRepository.findById(3L).get());
        user.setActive(true);

    }

    @Override
    public List<UserEntity> allUsers() {
        return (List<UserEntity>) userRepository.findAll();
    }

    @Override
    public List<UserEntity> usersNotIn(Long id) {
        return userRepository.findUsersWhereNotTaskInner(id);
    }

    @Override
    public List<UserEntity> usersIn(Long id) {
        return userRepository.findUsersWhereTaskInner(id);
    }

    @Override
    public List<UserEntity> usersIncomNotIn(Long id) {
        return userRepository.findUsersWhereNotTaskIncoming(id);
    }

    @Override
    public List<UserEntity> usersIncomIn(Long id) {
        return userRepository.findUsersWhereTaskIncoming(id);
    }
}
