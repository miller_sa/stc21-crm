package crm.stc21.service;

import crm.stc21.entity.InnerboxEntity;
import crm.stc21.entity.InnerboxTypeNameEntity;

import java.util.List;

public interface IInnerboxTypeNameService {
    List<InnerboxTypeNameEntity> findAllTypes();
}
