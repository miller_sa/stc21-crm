package crm.stc21.service;

import crm.stc21.entity.OutBoxEntity;

import java.util.List;

public interface IOutBoxService {
    List<OutBoxEntity> findAllOutBox();
    Long saveOutBox(OutBoxEntity outBoxEntity);
    OutBoxEntity findbyIdOutBox (Long id);
    void deleteOutBox(Long id);
}
