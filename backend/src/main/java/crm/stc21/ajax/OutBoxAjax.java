package crm.stc21.ajax;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import crm.stc21.entity.OutBoxEntity;
import crm.stc21.reference.OutBoxPermissionsRef;
import crm.stc21.service.OutBoxService;
import crm.stc21.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/ajax")
public class OutBoxAjax {
    OutBoxService outBoxService;
    UserService userService;
    OutBoxPermissionsRef outBoxPermissionsRef;

    @RequestMapping(
            value = "/allOutbox",
            method = RequestMethod.POST,
            produces = {MimeTypeUtils.APPLICATION_JSON_VALUE})
    public ResponseEntity getAllOutBox() {
        try {
            List<OutBoxEntity> allData = outBoxService.findAllOutBox();
            ObjectMapper map = new ObjectMapper();
            ArrayNode arrayAllData = map.createArrayNode();
            for (OutBoxEntity field : allData) {
                ObjectNode toJsonLineData = map.createObjectNode();
                toJsonLineData.put("id", field.getId());
                toJsonLineData.put("year", field.getYear());
                toJsonLineData.put("numb", field.getNumber());
                toJsonLineData.put("title", field.getTitle());
                String deleteItem = "";
                String editItem = "";
                String showItem = "";

                if (userService.isAuthUserHasPermission(outBoxPermissionsRef.getOutBoxDeletePermission())) {
                    deleteItem = "<a href=\"#\" class=\"dropdown-item\"><i class=\"icon-cross2\"></i> Удалить</a>\n";
                }

                if (userService.isAuthUserHasPermission(outBoxPermissionsRef.getOutBoxEditPermission())) {
                    editItem = "<a href=\"/outbox/edit/" + field.getId() + "\" class=\"dropdown-item\"><i class=\"icon-pencil7\"></i> Редактирование</a>\n";
                }

                showItem = "<a href=\"/outbox/show/" + field.getId() + "\">" + field.getTitle() + "</a>";
                toJsonLineData.put("title", showItem);
                toJsonLineData.put("action", "<div class=\"list-icons\">\n" +
                        "<div class=\"dropdown\">\n" +
                        "<a href=\"#\" class=\"list-icons-item\" data-toggle=\"dropdown\">\n" +
                        "<i class=\"icon-menu9\"></i>\n" +
                        "</a>" +
                        "<div class=\"dropdown-menu dropdown-menu-right\">\n" +
                        "<a href=\"/outbox/edit/" + field.getId() + "\" class=\"dropdown-item\"><i class=\"icon-pencil7\"></i> Редактирование</a>\n" +
                        "<a href=\"/outbox/delete/" + field.getId() + "\" class=\"dropdown-item\"><i class=\"icon-cross2\"></i> Удалить</a>\n" +
                        "</div>");
                arrayAllData.add(toJsonLineData);
            }

            ObjectMapper mapper = new ObjectMapper();
            ObjectNode allJsonOut = mapper.createObjectNode();
            allJsonOut.putPOJO("data", arrayAllData);
            //System.out.println(allJsonOut);
            return new ResponseEntity(allJsonOut, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }
    @Autowired
    public void setOutBoxService(OutBoxService outBoxService) {
        this.outBoxService = outBoxService;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setOutBoxPermissionsRef(OutBoxPermissionsRef outBoxPermissionsRef) {
        this.outBoxPermissionsRef = outBoxPermissionsRef;
    }
}