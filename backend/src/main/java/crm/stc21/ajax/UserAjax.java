package crm.stc21.ajax;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import crm.stc21.entity.UserEntity;
import crm.stc21.reference.TaskPermissionsRef;
import crm.stc21.reference.TaskStatusRef;
import crm.stc21.service.TaskService;
import crm.stc21.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;


@Controller
@RequestMapping("/ajax")
public class UserAjax {

    UserService userService;

    @RequestMapping(
            value = "/allUsers",
            method = RequestMethod.POST,
            produces = {MimeTypeUtils.APPLICATION_JSON_VALUE})
    public ResponseEntity getAllUsers() {
        String accept = "Ошибка!";
        try {
            List<UserEntity> allData = userService.getAllActiveUsers();
            ObjectMapper map = new ObjectMapper();
            ArrayNode arrayAllData = map.createArrayNode();
            for (UserEntity field : allData) {
                ObjectNode toJsonLineData = map.createObjectNode();
                toJsonLineData.put("id", field.getId());
                toJsonLineData.put("login", "<a href=\"/users/edit/" + field.getId() + "\">" + field.getLogin() + "</a>");
                toJsonLineData.put("firstname", field.getFirstName());
                toJsonLineData.put("lastname", field.getLastName());
//                String editItem = "";
//                if (userService.isAuthUserHasPermission(taskPermissionsRef.getTaskEditPermission())
//                        && field.getTaskStatus().equals(taskStatusRef.getDraftTaskStatus())
//                        || userService.isAuthUserHasPermission(taskPermissionsRef.getTaskEditPermission())
//                        && !userService.isAuthUserHasPermission(taskPermissionsRef.getTaskListAllPermission())
//                        && (field.getUserCreated().getId().equals(userService.getAuthUser().getId())
//                        && field.getTaskStatus().equals(taskStatusRef.getDraftTaskStatus()))
//                ) {
//                    editItem = "<a href=\"/users/edit/" + field.getId() + "\" class=\"dropdown-item\"><i class=\"icon-pencil7\"></i> Редактирование</a>\n";
//                }
//                ;
//                String deleteItem = "";
//                if (userService.isAuthUserHasPermission(taskPermissionsRef.getTaskDeletePermission())) {
//                    deleteItem = "<a href=\"/users/delete/" + field.getId() + "\" class=\"dropdown-item\"><i class=\"icon-cross2\"></i> Удалить</a>\n";
//                }
                ;
//                toJsonLineData.put("action", "<div class=\"list-icons\">\n" +
//                        "<div class=\"dropdown\">\n" +
//                        "<a href=\"#\" class=\"list-icons-item\" data-toggle=\"dropdown\">\n" +
//                        "<i class=\"icon-menu9\"></i>\n" +
//                        "</a>" +
//                        "<div class=\"dropdown-menu dropdown-menu-right\">\n" +
//                        editItem +
//                        deleteItem +
//                        "</div>");
                arrayAllData.add(toJsonLineData);
            }

            ObjectMapper mapper = new ObjectMapper();
            ObjectNode allJsonOut = mapper.createObjectNode();
            allJsonOut.putPOJO("data", arrayAllData);
            return new ResponseEntity(allJsonOut, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

}

