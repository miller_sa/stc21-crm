package crm.stc21.ajax;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import crm.stc21.entity.InnerboxEntity;
import crm.stc21.entity.TaskEntity;
import crm.stc21.reference.InnerBoxPermissionsRef;
import crm.stc21.reference.TaskStatusRef;
import crm.stc21.repository.InnerboxRepository;
import crm.stc21.repository.TaskRepository;
import crm.stc21.service.InnerboxService;
import crm.stc21.service.TaskService;
import crm.stc21.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


@RestController
@RequestMapping("/ajax")
public class InnerboxAjax {

    private InnerboxService innerboxService;
    private UserService userService;
    private InnerBoxPermissionsRef innerBoxPermissionsRef;
    @Autowired
    TaskStatusRef taskStatusRef;
    @Autowired
    private TaskRepository taskRepository;
    @Autowired
    TaskService taskService;
    @Autowired
    InnerboxRepository innerboxRepository;

    @RequestMapping(
            value = "/allInnerbox",
            method = RequestMethod.POST,
            produces = {MimeTypeUtils.APPLICATION_JSON_VALUE})
    public ResponseEntity getAllInnerbox() {
        try {
            List<InnerboxEntity> allData = innerboxRepository.findByIsDeletedFalse();
            ObjectMapper map = new ObjectMapper();
            ArrayNode arrayAllData = map.createArrayNode();
            for (InnerboxEntity field : allData) {
                ObjectNode toJsonLineData = map.createObjectNode();
                toJsonLineData.put("id", field.getId());
                toJsonLineData.put("number", field.getNumber());
                toJsonLineData.put("dataInner", field.getDataAdoption().toString());

                String deleteItem = "";
                String editItem = "";
                String showItem = "";

                if (userService.isAuthUserHasPermission(innerBoxPermissionsRef.getInnerBoxDeletePermission())) {
                    deleteItem = "<a href=\"/inner/delete/" + field.getId() + "\" class=\"dropdown-item\"><i class=\"icon-cross2\"></i>Удалить</a>\n";
                }

                if (userService.isAuthUserHasPermission(innerBoxPermissionsRef.getInnerBoxEditPermission())) {
                    editItem = "<a href=\"/inner/edit/" + field.getId() + "\" class=\"dropdown-item\"><i class=\"icon-pencil7\"></i>Редактировать</a>\n";
                }
                if (field.getDeadLine() != null) {
                    toJsonLineData.put("deadline", getHtmlstrDeadline(field.getDeadLine().toString()));
                } else {
                    toJsonLineData.put("deadline", "---- -- --");
                }
                showItem = "<a href=\"/inner/show/" + field.getId() + "\">" + field.getTitle() + "</a>";
                toJsonLineData.put("title", showItem);
                toJsonLineData.put("action", "<div class=\"list-icons\">\n" +
                        "<div class=\"dropdown\">\n" +
                        "<a href=\"#\" class=\"list-icons-item\" data-toggle=\"dropdown\">\n" +
                        "<i class=\"icon-menu9\"></i>\n" +
                        "</a>" +
                        "<div class=\"dropdown-menu dropdown-menu-right\">\n" +
                        editItem +
                        deleteItem +
                        "</div>");
                arrayAllData.add(toJsonLineData);
            }

            ObjectMapper mapper = new ObjectMapper();
            ObjectNode allJsonOut = mapper.createObjectNode();
            allJsonOut.putPOJO("data", arrayAllData);
            //  System.out.println(allJsonOut);
            return new ResponseEntity(allJsonOut, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/sendTaskToUser",
            produces = {MimeTypeUtils.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> sendTaskToUser(@RequestBody Long id) {
        try {
            TaskEntity taskEntity = taskService.findById(id);
            if (taskEntity.getTaskStatus().equals(taskStatusRef.getDraftTaskStatus())) {
                taskEntity.setTaskStatus(taskStatusRef.getAssignedTaskStatus());
                taskService.saveTask(taskEntity);
            }
            String result = "Задача отправлена исполнителю!";
            return new ResponseEntity(result, HttpStatus.OK);
        } catch (Exception e) {
            String result = "Ошибка";
            return ResponseEntity.badRequest().body(result);
        }
    }

    @Autowired
    public void setInnerboxService(InnerboxService innerboxService) {
        this.innerboxService = innerboxService;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setInnerBoxPermissionsRef(InnerBoxPermissionsRef innerBoxPermissionsRef) {
        this.innerBoxPermissionsRef = innerBoxPermissionsRef;
    }

    /**
     * За 3 дня до дедлайна выделяет дату с дедлайном.
     *
     * @param str
     * @return
     */
    static String getHtmlstrDeadline(String str) throws ParseException {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date dateDeadLine = dateFormat.parse(str);
        Date dateNow = dateFormat.parse("2019-12-25");
        long milliseconds = dateNow.getTime() - dateDeadLine.getTime();
        // 24 часа = 1 440 минут = 1 день
        int days = (int) (milliseconds / (24 * 60 * 60 * 1000));
        // if (days <= 3) { return  str + "  <span class=\"badge bg-warning-400 rounded-circle badge-icon\"><i class=\"icon-bell3\"></i></span>";}
        if (days <= 3) {
            return "<span class=\"badge badge-light badge-striped badge-striped-left border-left-warning mildoc\"> " + str + "</span>";
        } else {
            return str;
        }
        // System.out.println("Разница между датами в днях: " + days);
    }


}
