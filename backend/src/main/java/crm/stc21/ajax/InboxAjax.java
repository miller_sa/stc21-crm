package crm.stc21.ajax;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import crm.stc21.entity.IncomingBoxEntity;
import crm.stc21.reference.IncomingBoxPermissionsRef;
import crm.stc21.reference.TaskStatusRef;
import crm.stc21.repository.IncomingBoxRepository;
import crm.stc21.repository.TaskRepository;
import crm.stc21.service.IncomingBoxService;
import crm.stc21.service.TaskService;
import crm.stc21.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/ajax")
public class InboxAjax {

    private IncomingBoxService incomingBoxService;
    private UserService userService;
    private IncomingBoxPermissionsRef incomingBoxPermissionsRef;
    private TaskStatusRef taskStatusRef;
    private TaskRepository taskRepository;
    private TaskService taskService;
    private IncomingBoxRepository incomingBoxRepository;

    @RequestMapping(
            value = "/allInbox",
            method = RequestMethod.POST,
            produces = {MimeTypeUtils.APPLICATION_JSON_VALUE})
    public ResponseEntity getAllInbox() {
        String accept = "Ошибка!";
        try {
            List<IncomingBoxEntity> allData = incomingBoxService.findAllIncomingBox();
            ObjectMapper map = new ObjectMapper();
            ArrayNode arrayAllData = map.createArrayNode();
            for (IncomingBoxEntity field : allData) {
                ObjectNode toJsonLineData = map.createObjectNode();
                toJsonLineData.put("id", field.getId());
                toJsonLineData.put("year", field.getYear());
                toJsonLineData.put("numb", field.getNumber());
                toJsonLineData.put("title", field.getTitle());
                String deleteItem = "";
                String editItem = "";
                String showItem = "";

                if (userService.isAuthUserHasPermission(incomingBoxPermissionsRef.getIncomingBoxDelete())) {
                    deleteItem = "<a href=\"/inbox/delete/" + field.getId() + "\" class=\"dropdown-item\"><i class=\"icon-cross2\"></i> Удалить</a>\n";
                }
                ;

                if (userService.isAuthUserHasPermission(incomingBoxPermissionsRef.getIncomingBoxEdit())) {
                    editItem = "<a href=\"/inbox/edit/" + field.getId() + "\" class=\"dropdown-item\"><i class=\"icon-pencil7\"></i> Редактирование</a>\n";
                }
                if (field.getDeadLine() != null) {
                    toJsonLineData.put("deadline", InnerboxAjax.getHtmlstrDeadline(field.getDeadLine().toString()));
                } else {
                    toJsonLineData.put("deadline", "---- -- --");
                }

                showItem = "<a href=\"/inbox/show/" + field.getId() + "\">" + field.getTitle() + "</a>";

                toJsonLineData.put("title", showItem);
                toJsonLineData.put("action", "<div class=\"list-icons\">\n" +
                        "<div class=\"dropdown\">\n" +
                        "<a href=\"#\" class=\"list-icons-item\" data-toggle=\"dropdown\">\n" +
                        "<i class=\"icon-menu9\"></i>\n" +
                        "</a>" +
                        "<div class=\"dropdown-menu dropdown-menu-right\">\n" +
                        editItem +
                        deleteItem +
                        "</div>");
                arrayAllData.add(toJsonLineData);
            }

            ObjectMapper mapper = new ObjectMapper();
            ObjectNode allJsonOut = mapper.createObjectNode();
            allJsonOut.putPOJO("data", arrayAllData);
            return new ResponseEntity(allJsonOut, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @Autowired
    public void setIncomingBoxService(IncomingBoxService incomingBoxService) {
        this.incomingBoxService = incomingBoxService;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setIncomingBoxPermissionsRef(IncomingBoxPermissionsRef incomingBoxPermissionsRef) {
        this.incomingBoxPermissionsRef = incomingBoxPermissionsRef;
    }

    @Autowired
    public void setTaskStatusRef(TaskStatusRef taskStatusRef) {
        this.taskStatusRef = taskStatusRef;
    }

    @Autowired
    public void setTaskRepository(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Autowired
    public void setTaskService(TaskService taskService) {
        this.taskService = taskService;
    }

    @Autowired
    public void setIncomingBoxRepository(IncomingBoxRepository incomingBoxRepository) {
        this.incomingBoxRepository = incomingBoxRepository;
    }
}

