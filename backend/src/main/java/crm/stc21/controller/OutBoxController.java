package crm.stc21.controller;

import crm.stc21.controller.errors.Error403;
import crm.stc21.entity.InnerboxEntity;
import crm.stc21.entity.InnerboxTypeNameEntity;
import crm.stc21.entity.OutBoxEntity;
import crm.stc21.entity.UserEntity;
import crm.stc21.reference.OutBoxPermissionsRef;
import crm.stc21.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping("/outbox")
public class OutBoxController {

    private IOutBoxService outBoxService;
    private UserService userService;
    private OutBoxPermissionsRef outBoxPermissionsRef;
    @Autowired
    RegisterServiceImpl registerServiceImpl;

    @Autowired
    public OutBoxController(IOutBoxService outBoxService) {
        this.outBoxService = outBoxService;
    }

    @GetMapping("")
    public String findOutBox(Model model) {
        if (!userService.isAuthUserHasPermission(outBoxPermissionsRef.getOutBoxListAllPermission())) {
            model.addAttribute(Error403.ERROR_MESSAGE_PARAM_NAME, outBoxPermissionsRef.getOutBoxListAllPermission().getErrorMessage());
            return Error403.ERROR_403_VIEW;
        }
        return "outbox/index";
    }

    @GetMapping({"/edit", "/edit/{id}"})
    public String updateOutBox(@PathVariable Optional<Long> id, Model model) {
        OutBoxEntity outBoxEntity ;
        String action;
        if (id.isPresent()) {
            if (!userService.isAuthUserHasPermission(outBoxPermissionsRef.getOutBoxEditPermission())) {
                model.addAttribute(Error403.ERROR_MESSAGE_PARAM_NAME, outBoxPermissionsRef.getOutBoxEditPermission().getErrorMessage());
                return Error403.ERROR_403_VIEW;
            }
            action = "Изменение";
            outBoxEntity = outBoxService.findbyIdOutBox(id.get());
            Long year = outBoxEntity.getYear();
            model.addAttribute("years", year);

        } else {
            if (!userService.isAuthUserHasPermission(outBoxPermissionsRef.getOutBoxAddPermission())) {
                model.addAttribute(Error403.ERROR_MESSAGE_PARAM_NAME, outBoxPermissionsRef.getOutBoxAddPermission().getErrorMessage());
                return Error403.ERROR_403_VIEW;
            }
            if (isJanuary()) {
                Integer year = getNowYear();
                Set<Integer> years = new HashSet<>();
                years.add(year - 1);
                years.add(year);
                model.addAttribute("years", years);
            } else {
                Integer year = getNowYear();
                Set<Integer> years = new HashSet<>();
                years.add(year);
                model.addAttribute("years", years);
            }
            outBoxEntity = new OutBoxEntity();
            outBoxEntity.setYear(OutBoxService.getCurrentYear());
            action = "Добавление";
        }
        model.addAttribute("outBoxEntity", outBoxEntity);
        model.addAttribute("id", outBoxEntity.getId());
        model.addAttribute("action", action);
        return "outbox/edit";
    }

    @PostMapping({"/update/", "/update/{id}"})
    public String updateUser(@Valid OutBoxEntity outBoxEntity,
                             BindingResult result, Model model) {
        if (!userService.isAuthUserHasPermission(outBoxPermissionsRef.getOutBoxAddPermission())
                && !userService.isAuthUserHasPermission(outBoxPermissionsRef.getOutBoxEditPermission())) {
            model.addAttribute(Error403.ERROR_MESSAGE_PARAM_NAME, outBoxPermissionsRef.getOutBoxEditPermission().getErrorMessage());
            return Error403.ERROR_403_VIEW;
        }
        Long id = outBoxService.saveOutBox(outBoxEntity);
        return "redirect:/outbox";
    }

    @GetMapping("/delete/{id}")
    public String deleteTask(@PathVariable Long id, Model model) {
        if (!userService.isAuthUserHasPermission(outBoxPermissionsRef.getOutBoxDeletePermission())) {
            model.addAttribute(Error403.ERROR_MESSAGE_PARAM_NAME, outBoxPermissionsRef.getOutBoxDeletePermission().getErrorMessage());
            return Error403.ERROR_403_VIEW;
        }
        outBoxService.deleteOutBox(id);
        return "redirect:/outbox";
    }
    private void allDataForView(Model model) {
        List<UserEntity> allUsers = registerServiceImpl.allUsers();
        List<OutBoxEntity> allOutBox = outBoxService.findAllOutBox();
        model.addAttribute("allUsers", allUsers);
        model.addAttribute("allOutBox", allOutBox);
    }

    @GetMapping("/show/{id}")
    public String show(@PathVariable Long id, Model model) throws ParseException {
        OutBoxEntity outBoxEntity = outBoxService.findbyIdOutBox(id);
        String action = "Просмотр";
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String createdAt = dateFormat.format(outBoxEntity.getCreatedAt());
        if (outBoxEntity.getUpdatedAt() != null) {
            String updatedAt = dateFormat.format(outBoxEntity.getUpdatedAt());
            model.addAttribute("updatedAt", updatedAt);
        }
        model.addAttribute("outbox", outBoxEntity);
        model.addAttribute("action", action);
        model.addAttribute("createdAt", createdAt);
        model.addAttribute("updatedAt", "");

        return "outbox/show";
    }
    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setOutBoxPermissionsRef(OutBoxPermissionsRef outBoxPermissionsRef) {
        this.outBoxPermissionsRef = outBoxPermissionsRef;
    }

        protected Boolean isJanuary() {
            Calendar calendar = new GregorianCalendar();
            Integer month = calendar.get(Calendar.MONTH);
            return (month.equals(0)) ? true : false;
        }

        private Integer getNowYear() {
            Calendar calendar = new GregorianCalendar();
            return calendar.get(Calendar.YEAR);
        }
}
