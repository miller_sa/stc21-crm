package crm.stc21.controller;

import crm.stc21.entity.UserEntity;
import crm.stc21.service.UserPageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class UserPageController {
    UserPageService userPageService;

    @GetMapping("/userpage")
    public String index(Model model) {
        model.addAttribute("user", userPageService.getUser());
        model.addAttribute("error", "");
        return "userpage/userpage";
    }

    @PostMapping("/userpage")
    public String updateUser(
            @ModelAttribute("user") UserEntity user,
            Model model) {
        if (!user.getPassword().equals(user.getConfirmPassword())) {
            return "redirect:/userpage";
        }
        userPageService.updateUser(user, userPageService.getUser());
        return "redirect:/userpage";
    }


    @Autowired
    public void setUserPageService(UserPageService userPageService) {
        this.userPageService = userPageService;
    }
}
