package crm.stc21.controller.errors;

public class Error403 {
    public final static String ERROR_403_VIEW = "errors/403";
    public final static String ERROR_MESSAGE_PARAM_NAME = "errorMessage";
}
