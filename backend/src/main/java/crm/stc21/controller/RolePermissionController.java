package crm.stc21.controller;

import crm.stc21.entity.RoleEntity;
import crm.stc21.entity.UserEntity;
import crm.stc21.service.IRoleService;
import crm.stc21.service.PermissionService;
import crm.stc21.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.Optional;


@Controller
@RequestMapping("/roles")
public class RolePermissionController {

    private UserService userService;
    private IRoleService roleService;
    private PermissionService permissionService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setRoleService(IRoleService roleService) {
        this.roleService = roleService;
    }

    @Autowired
    public void setPermissionService(PermissionService permissionService) {
        this.permissionService = permissionService;
    }

    @GetMapping("")
    public String getRoles(Model model) {
        return "roles/roles";
    }

    @GetMapping({"/edit", "/edit/{id}"})
    public String editRole(@PathVariable Optional<Long> id, Model model) {
        RoleEntity roleEntity;
        String action;
        if (id.isPresent()) {
            roleEntity = roleService.getRoleById(id.get());
            action = "Редактирование";
        } else {
            roleEntity = new RoleEntity();
            action = "Добавление";
        }

        model.addAttribute("allPermissions", permissionService.getPermissions());
        model.addAttribute("role", roleEntity);
        model.addAttribute("action", action);
        model.addAttribute("id", roleEntity.getId());
        return "roles/roleEdit";
    }

    @PostMapping({"/updateRole/", "/updateRole/{id}"})
    public String updateUser(@Valid RoleEntity roleEntity) {
        roleService.saveRole(roleEntity);
        return "redirect:/roles";
    }

}
