package crm.stc21.controller;

import crm.stc21.controller.errors.Error403;
import crm.stc21.entity.TaskEntity;
import crm.stc21.reference.TaskPermissionsRef;
import crm.stc21.reference.TaskStatusRef;
import crm.stc21.service.ITaskService;
import crm.stc21.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping("/tasks")
public class TaskController {
    private ITaskService taskService;
    private UserService userService;
    private TaskPermissionsRef taskPermissionsRef;
    private TaskStatusRef taskStatusRef;

    @GetMapping("")
    public String getTasks(Model model) {
        if (!userService.isAuthUserHasPermission(taskPermissionsRef.getTaskListAllPermission())
                && !userService.isAuthUserHasPermission(taskPermissionsRef.getTaskListMyPermission())) {
            model.addAttribute(Error403.ERROR_MESSAGE_PARAM_NAME, taskPermissionsRef.getTaskListAllPermission().getErrorMessage());
            return Error403.ERROR_403_VIEW;
        }
        return "tasks/index";
    }

//    @GetMapping("/my")
//    public String getTasksMy(Model model) {
//        if (!userService.isAuthUserHasPermission(taskPermissionsRef.getTaskListMyPermission())) {
//            model.addAttribute(Error403.ERROR_MESSAGE_PARAM_NAME, taskPermissionsRef.getTaskListMyPermission().getErrorMessage());
//            return Error403.ERROR_403_VIEW;
//        }
//        return "tasks/index";
//    }


    @GetMapping("/show/{id}")
    public String showTask(@PathVariable Long id, Model model) {
        if (!userService.isAuthUserHasPermission(taskPermissionsRef.getTaskShowPermission())) {
            model.addAttribute(Error403.ERROR_MESSAGE_PARAM_NAME, taskPermissionsRef.getTaskShowPermission().getErrorMessage());
            return Error403.ERROR_403_VIEW;
        }

        TaskEntity taskEntity = taskService.findById(id);

        //User can view only his tasks
        if (!userService.isAuthUserHasPermission(taskPermissionsRef.getTaskListAllPermission())
                && !taskEntity.getUser().getId().equals(userService.getAuthUser().getId())) {
            model.addAttribute(Error403.ERROR_MESSAGE_PARAM_NAME, taskPermissionsRef.getTaskShowPermission().getErrorMessage());
            return Error403.ERROR_403_VIEW;
        }

        model.addAttribute("canAcceptReject", taskService.isCanAuthUserAcceptRejectThisTask(taskEntity));
        model.addAttribute("canDone", taskService.isCanAuthUserDoneThisTask(taskEntity));
        model.addAttribute("taskEntity", taskEntity);
        return "tasks/show";
    }

    @GetMapping({"/edit", "/edit/{id}"})
    public String updateTask(@PathVariable Optional<Long> id, Model model) {
        TaskEntity taskEntity;
        String action;
        if (id.isPresent()) {

            if (!userService.isAuthUserHasPermission(taskPermissionsRef.getTaskEditPermission())) {
                model.addAttribute(Error403.ERROR_MESSAGE_PARAM_NAME, taskPermissionsRef.getTaskEditPermission().getErrorMessage());
                return Error403.ERROR_403_VIEW;
            }

            taskEntity = taskService.findById(id.get());

            if (!taskEntity.getTaskStatus().equals(taskStatusRef.getDraftTaskStatus())) {
                model.addAttribute(Error403.ERROR_MESSAGE_PARAM_NAME, taskPermissionsRef.getTaskEditPermission().getErrorMessage());
                return Error403.ERROR_403_VIEW;
            }

            //User can edit only his tasks
            if (!userService.isAuthUserHasPermission(taskPermissionsRef.getTaskListAllPermission())
                    && !taskEntity.getUserCreated().getId().equals(userService.getAuthUser().getId())) {
                model.addAttribute(Error403.ERROR_MESSAGE_PARAM_NAME, taskPermissionsRef.getTaskEditPermission().getErrorMessage());
                return Error403.ERROR_403_VIEW;
            }

            action = "Изменение";

        } else {

            if (!userService.isAuthUserHasPermission(taskPermissionsRef.getTaskAddPermission())) {
                model.addAttribute(Error403.ERROR_MESSAGE_PARAM_NAME, taskPermissionsRef.getTaskAddPermission().getErrorMessage());
                return Error403.ERROR_403_VIEW;
            }
            taskEntity = new TaskEntity();

            action = "Добавление";
        }


        model.addAttribute("allUsers", userService.getAllActiveUsers());
        model.addAttribute("taskEntity", taskEntity);
        model.addAttribute("id", taskEntity.getId());
        model.addAttribute("action", action);
        return "tasks/edit";
    }

    @PostMapping({"/update/", "/update/{id}"})
    public String updateTask(@Valid TaskEntity taskEntity,
                             BindingResult result, Model model) {
        if (!userService.isAuthUserHasPermission(taskPermissionsRef.getTaskAddPermission())
                && !userService.isAuthUserHasPermission(taskPermissionsRef.getTaskEditPermission())
        ) {
            model.addAttribute(Error403.ERROR_MESSAGE_PARAM_NAME, taskPermissionsRef.getTaskAddPermission().getErrorMessage());
            return Error403.ERROR_403_VIEW;
        }
        if (taskEntity.getTaskStatus() == null) {
            taskEntity.setTaskStatus(taskStatusRef.getDraftTaskStatus());
        }

        Long id = taskService.saveTask(taskEntity);
        return "redirect:/tasks";
    }

    @GetMapping("/delete/{id}")
    public String deleteTask(@PathVariable Long id, Model model) {
        if (!userService.isAuthUserHasPermission(taskPermissionsRef.getTaskDeletePermission())) {
            model.addAttribute(Error403.ERROR_MESSAGE_PARAM_NAME, taskPermissionsRef.getTaskDeletePermission().getErrorMessage());
            return Error403.ERROR_403_VIEW;
        }

        TaskEntity taskEntity = taskService.findById(id);

        //Можно удалять любые задачи если есть роль ROLE_OP_TASK_LIST_ALL иначе только свои
        if (!userService.isAuthUserHasPermission(taskPermissionsRef.getTaskListAllPermission())
                && (!taskEntity.getUserCreated().getId().equals(userService.getAuthUser().getId()))) {
            model.addAttribute(Error403.ERROR_MESSAGE_PARAM_NAME, taskPermissionsRef.getTaskDeletePermission().getErrorMessage());
            return Error403.ERROR_403_VIEW;
        }

        taskService.deleteTask(id);
        return "redirect:/tasks";
    }

    @PostMapping("/accept/{id}")
    String acceptTask(@PathVariable Long id, Model model) {
        TaskEntity taskEntity = taskService.findById(id);

        if (!taskService.isCanAuthUserAcceptRejectThisTask(taskEntity)) {
            model.addAttribute(Error403.ERROR_MESSAGE_PARAM_NAME, taskPermissionsRef.getTaskProcessPermission().getErrorMessage());
            return Error403.ERROR_403_VIEW;
        }
        taskService.acceptTask(id);

        return "redirect:/tasks/show/" + id;
    }

    @PostMapping("/reject/{id}")
    String rejectTask(@PathVariable Long id, Model model) {
        TaskEntity taskEntity = taskService.findById(id);

        if (!taskService.isCanAuthUserAcceptRejectThisTask(taskEntity)) {
            model.addAttribute(Error403.ERROR_MESSAGE_PARAM_NAME, taskPermissionsRef.getTaskProcessPermission().getErrorMessage());
            return Error403.ERROR_403_VIEW;
        }
        taskService.rejectTask(id);
        return "redirect:/tasks/show/" + id;
    }

    @PostMapping("/done/{id}")
    String doneTask(@PathVariable Long id, Model model) {
        TaskEntity taskEntity = taskService.findById(id);

        if (!taskService.isCanAuthUserDoneThisTask(taskEntity)) {
            model.addAttribute(Error403.ERROR_MESSAGE_PARAM_NAME, taskPermissionsRef.getTaskProcessPermission().getErrorMessage());
            return Error403.ERROR_403_VIEW;
        }
        taskService.doneTask(id);
        return "redirect:/tasks/show/" + id;
    }

    @PostMapping({"/assign/", "/assign/{id}"})
    public String assignTask(@Valid TaskEntity taskEntity,
                             BindingResult result, Model model) {
        if (taskEntity.getTaskStatus() == null) {
            taskEntity.setTaskStatus(taskStatusRef.getDraftTaskStatus());
        }

        if (!taskService.isCanAuthUserAssignedThisTask(taskEntity)) {
            model.addAttribute(Error403.ERROR_MESSAGE_PARAM_NAME, taskPermissionsRef.getTaskProcessPermission().getErrorMessage());
            return Error403.ERROR_403_VIEW;
        }
        taskEntity.setTaskStatus(taskStatusRef.getAssignedTaskStatus());
        String redirect = updateTask(taskEntity, result, model);
        return redirect;
    }


    @Autowired
    public void setTaskService(ITaskService taskService) {
        this.taskService = taskService;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setTaskPermissionsRef(TaskPermissionsRef taskPermissionsRef) {
        this.taskPermissionsRef = taskPermissionsRef;
    }

    @Autowired
    public void setTaskStatusRef(TaskStatusRef taskStatusRef) {
        this.taskStatusRef = taskStatusRef;
    }
}
