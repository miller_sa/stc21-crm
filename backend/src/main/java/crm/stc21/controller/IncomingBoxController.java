package crm.stc21.controller;

import crm.stc21.entity.IncomingBoxEntity;
import crm.stc21.entity.InnerboxEntity;
import crm.stc21.entity.InnerboxTypeNameEntity;
import crm.stc21.entity.UserEntity;
import crm.stc21.reference.IncomingBoxPermissionsRef;
import crm.stc21.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping("/inbox")
public class IncomingBoxController {

    IIncomingBoxService incomingBoxService;
    @Autowired
    UserService userService;
    IncomingBoxPermissionsRef incomingBoxPermissionsRef;
    @Autowired
    RegisterServiceImpl registerServiceImpl;
    @Autowired
    IInnerboxTypeNameService innerboxTypeNameService;
    @Autowired
    TaskService taskService;

    @Autowired
    public void setIncomingBoxPermissionsRef(IncomingBoxPermissionsRef incomingBoxPermissionsRef) {
        this.incomingBoxPermissionsRef = incomingBoxPermissionsRef;
    }

    @Autowired
    public void setIncomingBoxService(IIncomingBoxService incomingBoxService) {
        this.incomingBoxService = incomingBoxService;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("")
    public String getIncomingBox() {
        /*if (!userService.isAuthUserHasPermission(incomingBoxPermissionsRef.getIncomingBoxListAll())) {
            return "errors/403";
        }*/
        return "inbox/index";
    }

    @GetMapping({"/edit", "/edit/{id}"})
    public String updateTask(@PathVariable Optional<Long> id, Model model) {
        IncomingBoxEntity incomingBoxEntity;
        String action;
        if (id.isPresent()) {
            if (!userService.isAuthUserHasPermission(incomingBoxPermissionsRef.getIncomingBoxEdit())) {
                return "errors/403";
            }
            incomingBoxEntity = incomingBoxService.findIncomingBoxById(id.get());
            action = "Изменение";
            model.addAttribute("disabled", true);
            model.addAttribute("disableYear", true);
            Long year = incomingBoxEntity.getYear();
            Set<Long> years = new HashSet<>();
            years.add(year);
            model.addAttribute("years", years);
            if (!incomingBoxEntity.getTaskIncoming().isEmpty()) {
                List<UserEntity> usersNotIn = registerServiceImpl.usersIncomNotIn(id.get());
                List<UserEntity> usersIn = registerServiceImpl.usersIncomIn(id.get());
                model.addAttribute("usersNotIn", usersNotIn);
                model.addAttribute("usersIn", usersIn);
                List<UserEntity> allUsers = registerServiceImpl.allUsers();
                model.addAttribute("allUsers", allUsers);

            }
        } else {
            if (!userService.isAuthUserHasPermission(incomingBoxPermissionsRef.getIncomingBoxAdd())) {
                return "errors/403";
            }
            incomingBoxEntity = new IncomingBoxEntity();
            String disableYear;
            if (isJanuary()) {
                Integer year = getNowYear();
                model.addAttribute("disableYear", "false");
                Set<Integer> years = new HashSet<>();
                years.add(year - 1);
                years.add(year);
                model.addAttribute("years", years);
            } else {
                Integer year = getNowYear();
                model.addAttribute("disableYear", true);
                Set<Integer> years = new HashSet<>();
                years.add(year);
                model.addAttribute("years", years);
                List<UserEntity> allUsers = registerServiceImpl.allUsers();
                model.addAttribute("allUsers", allUsers);

            }
            model.addAttribute("disabled", false);
            action = "Добавление";
        }

        List<InnerboxTypeNameEntity> allTypes = innerboxTypeNameService.findAllTypes();
        model.addAttribute("allTypes", allTypes);
        model.addAttribute("incomingBoxEntity", incomingBoxEntity);
        model.addAttribute("id", incomingBoxEntity.getId());
        model.addAttribute("action", action);
        return "inbox/edit";
    }


    @GetMapping("/show/{id}")
    public String show(@PathVariable Long id, Model model) throws ParseException {
        IncomingBoxEntity incomingBoxEntity = incomingBoxService.findIncomingBoxById(id);
        String action = "Просмотр";
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String createdAt = dateFormat.format(incomingBoxEntity.getCreatedAt());
        if (incomingBoxEntity.getUpdatedAt() != null) {
            String updatedAt = dateFormat.format(incomingBoxEntity.getUpdatedAt());
            model.addAttribute("updatedAt", updatedAt);
        }
        model.addAttribute("incomingBox", incomingBoxEntity);
        model.addAttribute("action", action);
        model.addAttribute("createdAt", createdAt);
        model.addAttribute("updatedAt", "");
        return "inbox/show";
    }

    @PostMapping({"/update/", "/update/{id}"})
    public String updateUser(@Valid IncomingBoxEntity incomingBoxEntity, @PathVariable Optional<Long> id, Long executor,
                             BindingResult result, Model model) {
        Long idIncoming = incomingBoxService.saveIncomingBox(incomingBoxEntity);
        boolean update = false;

        if (model.getAttribute("id") == null && executor != null && !id.isPresent()) {
        /*    if (!userService.isAuthUserHasPermission(incomingBoxPermissionsRef.getIncomingBoxAdd())) {
                return "errors/403";
            }*/

            taskService.saveIncomingTask(executor, idIncoming, update);
        }
        if (executor != null && id.isPresent()) {
       /*     if (!userService.isAuthUserHasPermission(incomingBoxPermissionsRef.getIncomingBoxEdit())) {
                return "errors/403";
            }*/

            taskService.saveIncomingTask(executor, idIncoming, update = true);
        }

        return "redirect:/inbox";
    }

    @GetMapping("/delete/{id}")
    public String deleteInbox(@PathVariable Long id, Model model) {
        if (!userService.isAuthUserHasPermission(incomingBoxPermissionsRef.getIncomingBoxDelete())) {
            return "errors/403";
        }
        incomingBoxService.deleteIncomingBox(id);
        return "redirect:/inbox";
    }

    protected Boolean isJanuary() {
        Calendar calendar = new GregorianCalendar();
        Integer month = calendar.get(Calendar.MONTH);
        return month.equals(0);
    }

    private Integer getNowYear() {
        Calendar calendar = new GregorianCalendar();
        return calendar.get(Calendar.YEAR);
    }
}
