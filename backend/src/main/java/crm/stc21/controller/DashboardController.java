package crm.stc21.controller;

import crm.stc21.reference.TaskStatusRef;
import crm.stc21.repository.IncomingBoxRepository;
import crm.stc21.repository.InnerboxRepository;
import crm.stc21.repository.OutBoxRepository;
import crm.stc21.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class DashboardController {
    TaskRepository taskRepository;
    TaskStatusRef taskStatusRef;
    InnerboxRepository innerboxRepository;
    IncomingBoxRepository incomingBoxRepository;
    OutBoxRepository outBoxRepository;

    @GetMapping("/")
    public String index(Model model) {
        Long assignedTasksCount = taskRepository.countByTaskStatus(taskStatusRef.getAssignedTaskStatus());
        Long rejectedTasksCount = taskRepository.countByTaskStatus(taskStatusRef.getRejectedStatus());
        Long inWorkTasksCount = taskRepository.countByTaskStatus(taskStatusRef.getInWorkStatus());
        Long doneTasksCount = taskRepository.countByTaskStatus(taskStatusRef.getDoneStatus());
        Long incomingCount = incomingBoxRepository.count();
        Long innerCount = innerboxRepository.count();
        Long outCount = outBoxRepository.count();

        model.addAttribute("assignedTasksCount", assignedTasksCount);
        model.addAttribute("inWorkTasksCount", inWorkTasksCount);
        model.addAttribute("rejectedTasksCount", rejectedTasksCount);
        model.addAttribute("doneTasksCount", doneTasksCount);
        model.addAttribute("incomingCount", incomingCount);
        model.addAttribute("innerCount", innerCount);
        model.addAttribute("outCount", outCount);

        return "dashboard";
    }

    @GetMapping("/login")
    public String login(HttpServletRequest request, Model model) {
        String referrer = request.getHeader("Referer");
        request.getSession().setAttribute("url_prior_login", referrer);
        return "auth/login";
    }



    @GetMapping(value="/logout")
    public String logoutPage (HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/login";
    }

    @Autowired
    public void setTaskRepository(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Autowired
    public void setTaskStatusRef(TaskStatusRef taskStatusRef) {
        this.taskStatusRef = taskStatusRef;
    }

    @Autowired
    public void setInnerboxRepository(InnerboxRepository innerboxRepository) {
        this.innerboxRepository = innerboxRepository;
    }

    @Autowired
    public void setIncomingBoxRepository(IncomingBoxRepository incomingBoxRepository) {
        this.incomingBoxRepository = incomingBoxRepository;
    }

    @Autowired
    public void setOutBoxRepository(OutBoxRepository outBoxRepository) {
        this.outBoxRepository = outBoxRepository;
    }
}
