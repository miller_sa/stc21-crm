package crm.stc21.controller;

import crm.stc21.entity.RoleEntity;
import crm.stc21.entity.UserEntity;
import crm.stc21.service.IRoleService;
import crm.stc21.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping("/users")
public class UsersController {

    UserService userService;
    IRoleService roleService;

    @GetMapping("")
    public String getUsers(Model model) {
        return "roles/users";
    }

    @GetMapping({"/edit", "/edit/{id}"})
    public String editUser(@PathVariable Optional<Long> id, Model model) {
        UserEntity userEntity;
        String action;
        if (id.isPresent()) {
            userEntity = userService.findUserById(id.get());
            action = "Редактирование";
        } else {
            userEntity = new UserEntity();
            action = "Добавление";
        }

        model.addAttribute("roles", roleService.getAllRoles());
        model.addAttribute("user", userEntity);
        model.addAttribute("action", action);
        model.addAttribute("id", userEntity.getId());
        return "roles/userEdit";
    }

    @PostMapping({"/updateUser/", "/updateUser/{id}"})
    public String updateUser(@Valid UserEntity userEntity
                             ) {
        if (!userEntity.getPassword().equals(userEntity.getConfirmPassword())) {
            return "redirect:/users";
        }
//        System.out.println(roleEntity.getName());
        userService.saveUser(userEntity);
        return "redirect:/users";
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setRoleService(IRoleService roleService) {
        this.roleService = roleService;
    }
}
