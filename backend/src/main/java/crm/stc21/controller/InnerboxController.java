package crm.stc21.controller;

import crm.stc21.entity.InnerboxEntity;
import crm.stc21.entity.InnerboxTypeNameEntity;
import crm.stc21.entity.UserEntity;
import crm.stc21.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Optional;
import java.util.*;

@Controller
@RequestMapping("/inner")
public class InnerboxController {
    @Autowired
    IInnerboxService innerboxService;
    @Autowired
    RegisterServiceImpl registerServiceImpl;
    @Autowired
    IInnerboxTypeNameService innerboxTypeNameService;
    @Autowired
    TaskService taskService;

    @GetMapping("")
    public String index() {
        return "inner/index";
    }

    @GetMapping("/add")
    public String add(Model model) {
        InnerboxEntity innerboxEntity;
        String action;
        String disableYear;
        allDataForView(model);
        innerboxEntity = new InnerboxEntity();
        if (isJanuary()) {
            Integer year = getNowYear();
            model.addAttribute("disableYear", "false");
            Set<Integer> years = new HashSet<>();
            years.add(year - 1);
            years.add(year);
            model.addAttribute("years", years);
        } else {
            Integer year = getNowYear();
            model.addAttribute("disableYear", true);
            Set<Integer> years = new HashSet<>();
            years.add(year);
            model.addAttribute("years", years);

        }
        action = "Добавление";
        model.addAttribute("disabled", false);
        model.addAttribute("innerbox", innerboxEntity);
        model.addAttribute("action", action);
        return "inner/add";
    }

    @GetMapping({"/edit/{id}"})
    public String update(@PathVariable Optional<Long> id, Model model) {
        InnerboxEntity innerboxEntity;
        String action;
        innerboxEntity = innerboxService.findInnerById(id.get());
        allDataForView(model);
        model.addAttribute("disableYear", true);
        model.addAttribute("disabled", true);
        Long year = innerboxEntity.getYear();
        Set<Long> years = new HashSet<>();
        years.add(year);
        model.addAttribute("years", years);
        action = "Изменение";
        if (!innerboxEntity.getTasksInner().isEmpty()) {
            List<UserEntity> usersNotIn = registerServiceImpl.usersNotIn(id.get());
            List<UserEntity> usersIn = registerServiceImpl.usersIn(id.get());
            model.addAttribute("usersNotIn", usersNotIn);
            model.addAttribute("usersIn", usersIn);
        }

        model.addAttribute("innerbox", innerboxEntity);
        model.addAttribute("id", innerboxEntity.getId());
        model.addAttribute("action", action);
        return "inner/edit";
    }

    private void allDataForView(Model model) {
        List<InnerboxEntity> allInners = innerboxService.findAllInner();
        List<InnerboxTypeNameEntity> allTypes = innerboxTypeNameService.findAllTypes();
        List<UserEntity> allUsers = registerServiceImpl.allUsers();
        model.addAttribute("allUsers", allUsers);
        model.addAttribute("allInners", allInners);
        model.addAttribute("allTypes", allTypes);
    }

    @PostMapping({"/store/", "/store/{id}"})
    public String storeInner(@Valid InnerboxEntity innerboxEntity, @PathVariable Optional<Long> id, Long executor,
                             BindingResult result, Model model) {
        boolean update = false;
        Long idInner = innerboxService.saveInner(innerboxEntity);
        if (executor != null && !id.isPresent()) {
            taskService.saveInnerTask(executor, idInner, update);
        }
        if (executor != null && id.isPresent()) {

            taskService.saveInnerTask(executor, idInner, update = true);
        }

        return "redirect:/inner/";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Long id, Model model) {
        innerboxService.deleteInner(id);
        return "redirect:/inner";
    }

    @GetMapping("/show/{id}")
    public String show(@PathVariable Long id, Model model) throws ParseException {
        InnerboxEntity innerboxEntity = innerboxService.findInnerById(id);
        String action = "Просмотр";
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String createdAt = dateFormat.format(innerboxEntity.getCreatedAt());
        if (innerboxEntity.getUpdatedAt() != null) {
            String updatedAt = dateFormat.format(innerboxEntity.getUpdatedAt());
            model.addAttribute("updatedAt", updatedAt);
        }

        model.addAttribute("innerbox", innerboxEntity);
        model.addAttribute("action", action);
        model.addAttribute("createdAt", createdAt);
        model.addAttribute("updatedAt", "");
        return "inner/show";
    }

    protected Boolean isJanuary() {
        Calendar calendar = new GregorianCalendar();
        Integer month = calendar.get(Calendar.MONTH);
        return month.equals(0);
    }

    private Integer getNowYear() {
        Calendar calendar = new GregorianCalendar();
        return calendar.get(Calendar.YEAR);
    }
}
