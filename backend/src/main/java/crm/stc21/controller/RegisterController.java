package crm.stc21.controller;

import crm.stc21.entity.UserEntity;
import crm.stc21.repository.UserRepository;
import crm.stc21.service.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

@Controller
public class RegisterController {
    @Autowired
    UserRepository userRepository;
    @Autowired
    RegisterService registerService;

    @GetMapping("/register")
    public String registration(Model model) {
        model.addAttribute("userForm", new UserEntity());
        model.addAttribute("error", "nothing");
        return "/auth/register";
    }

    @PostMapping("/register")
    public String saveUser(
            @ModelAttribute("userForm") UserEntity userForm,
            BindingResult result,
            Model model) {
        String error = registerService.validation(userForm);
        if (!error.equals("true")) {
            model.addAttribute("error", error);
            return "/auth/register";
        }
        registerService.saveUser(userForm);
        System.out.println(userForm);
        userRepository.save(userForm);
        return "auth/login";
    }
}
