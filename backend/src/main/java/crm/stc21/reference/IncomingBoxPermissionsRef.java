package crm.stc21.reference;

import crm.stc21.entity.PermissionEntity;
import crm.stc21.repository.IncomingBoxRepository;
import crm.stc21.repository.PermissionsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class IncomingBoxPermissionsRef {
    private final PermissionsRepository permissionsRepository;

    private final PermissionEntity incomingBoxListAll;
    private final PermissionEntity incomingBoxListLinkedTasks;
    private final PermissionEntity incomingBoxAdd;
    private final PermissionEntity incomingBoxEdit;
    private final PermissionEntity incomingBoxDelete;

    @Autowired
    public IncomingBoxPermissionsRef(PermissionsRepository permissionsRepository) {
        this.permissionsRepository = permissionsRepository;
        this.incomingBoxListAll = permissionsRepository.findByName("ROLE_OP_INCOMING_BOX_LIST_ALL");
        this.incomingBoxListLinkedTasks = permissionsRepository.findByName("ROLE_OP_INCOMING_BOX_LIST_LINKED_TASKS");
        this.incomingBoxAdd = permissionsRepository.findByName("ROLE_OP_INCOMING_BOX_ADD");
        this.incomingBoxEdit = permissionsRepository.findByName("ROLE_OP_INCOMING_BOX_EDIT");
        this.incomingBoxDelete = permissionsRepository.findByName("ROLE_OP_INCOMING_BOX_DELETE");
    }

    public PermissionsRepository getPermissionsRepository() {
        return permissionsRepository;
    }

    public PermissionEntity getIncomingBoxListAll() {
        return incomingBoxListAll;
    }

    public PermissionEntity getIncomingBoxListLinkedTasks() {
        return incomingBoxListLinkedTasks;
    }

    public PermissionEntity getIncomingBoxAdd() {
        return incomingBoxAdd;
    }

    public PermissionEntity getIncomingBoxEdit() {
        return incomingBoxEdit;
    }

    public PermissionEntity getIncomingBoxDelete() {
        return incomingBoxDelete;
    }
}
