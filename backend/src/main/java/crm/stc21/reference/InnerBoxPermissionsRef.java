package crm.stc21.reference;

import crm.stc21.entity.PermissionEntity;
import crm.stc21.repository.PermissionsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class InnerBoxPermissionsRef {
    private PermissionsRepository permissionsRepository;

    private final PermissionEntity innerBoxListAllPermission;
    private final PermissionEntity innerBoxListLinkedTasksPermission;
    private final PermissionEntity innerBoxAddPermission;
    private final PermissionEntity innerBoxEditPermission;
    private final PermissionEntity innerBoxDeletePermission;

    @Autowired
    public InnerBoxPermissionsRef(PermissionsRepository permissionsRepository) {
        this.permissionsRepository = permissionsRepository;
        innerBoxListAllPermission = permissionsRepository.findByName("ROLE_OP_INNER_BOX_LIST_ALL");
        innerBoxListLinkedTasksPermission = permissionsRepository.findByName("ROLE_OP_INNER_BOX_LIST_LINKED_TASKS");
        innerBoxAddPermission = permissionsRepository.findByName("ROLE_OP_INNER_BOX_ADD");
        innerBoxEditPermission = permissionsRepository.findByName("ROLE_OP_INNER_BOX_EDIT");
        innerBoxDeletePermission = permissionsRepository.findByName("ROLE_OP_INNER_BOX_DELETE");
    }

    public PermissionEntity getInnerBoxListAllPermission() {
        return innerBoxListAllPermission;
    }

    public PermissionEntity getInnerBoxListLinkedTasksPermission() {
        return innerBoxListLinkedTasksPermission;
    }

    public PermissionEntity getInnerBoxAddPermission() {
        return innerBoxAddPermission;
    }

    public PermissionEntity getInnerBoxEditPermission() {
        return innerBoxEditPermission;
    }

    public PermissionEntity getInnerBoxDeletePermission() {
        return innerBoxDeletePermission;
    }
}
