package crm.stc21.reference;

import crm.stc21.entity.PermissionEntity;
import crm.stc21.repository.PermissionsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserPermissionsRef {
    PermissionsRepository permissionsRepository;
    PermissionEntity userEditPermissions;

    @Autowired
    public UserPermissionsRef(PermissionsRepository permissionsRepository) {
        this.permissionsRepository = permissionsRepository;
        this.userEditPermissions = permissionsRepository.findByName("ROLE_OP_USER_EDIT_PERMISSIONS");
    }
}
