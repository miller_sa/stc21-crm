package crm.stc21.reference;

import crm.stc21.entity.TaskStatusEntity;
import crm.stc21.repository.TaskStatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TaskStatusRef {
    private final TaskStatusEntity draftTaskStatus;
    private final TaskStatusEntity assignedTaskStatus;
    private final TaskStatusEntity inWorkStatus;
    private final TaskStatusEntity doneStatus;
    private final TaskStatusEntity rejectedStatus;
    private final TaskStatusEntity deletedStatus;

    TaskStatusRepository taskStatusRepository;

    @Autowired
    public TaskStatusRef(TaskStatusRepository taskStatusRepository) {
        this.taskStatusRepository = taskStatusRepository;
        this.draftTaskStatus = taskStatusRepository.findByName("ST_DRAFT").get();
        this.assignedTaskStatus = taskStatusRepository.findByName("ST_ASSIGNED").get();
        this.inWorkStatus = taskStatusRepository.findByName("ST_IN_WORK").get();
        this.doneStatus = taskStatusRepository.findByName("ST_DONE").get();
        this.rejectedStatus = taskStatusRepository.findByName("ST_REJECTED").get();
        this.deletedStatus = taskStatusRepository.findByName("ST_DELETED").get();
    }

    public TaskStatusEntity getDraftTaskStatus() {
        return draftTaskStatus;
    }

    public TaskStatusEntity getAssignedTaskStatus() {
        return assignedTaskStatus;
    }

    public TaskStatusEntity getInWorkStatus() {
        return inWorkStatus;
    }

    public TaskStatusEntity getDoneStatus() {
        return doneStatus;
    }

    public TaskStatusEntity getRejectedStatus() {
        return rejectedStatus;
    }

    public TaskStatusEntity getDeletedStatus() {
        return deletedStatus;
    }
}
