package crm.stc21.reference;

import crm.stc21.entity.PermissionEntity;
import crm.stc21.repository.PermissionsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TaskPermissionsRef {
    private final PermissionEntity taskAddPermission;
    private final PermissionEntity taskEditPermission;
    private final PermissionEntity taskDeletePermission;
    private final PermissionEntity taskProcessPermission;
    private final PermissionEntity taskListAllPermission;
    private final PermissionEntity taskListMyPermission;
    private final PermissionEntity taskShowPermission;

    PermissionsRepository permissionsRepository;

    @Autowired
    public TaskPermissionsRef(PermissionsRepository permissionsRepository) {
        this.permissionsRepository = permissionsRepository;
        this.taskAddPermission = permissionsRepository.findByName("ROLE_OP_TASK_ADD");
        this.taskEditPermission = permissionsRepository.findByName("ROLE_OP_TASK_EDIT");
        this.taskProcessPermission = permissionsRepository.findByName("ROLE_OP_TASK_PROCESS");
        this.taskListAllPermission = permissionsRepository.findByName("ROLE_OP_TASK_LIST_ALL");
        this.taskListMyPermission = permissionsRepository.findByName("ROLE_OP_TASK_LIST_MY");
        this.taskDeletePermission = permissionsRepository.findByName("ROLE_OP_TASK_DELETE");
        this.taskShowPermission = permissionsRepository.findByName("ROLE_OP_TASK_SHOW");
    }

    public PermissionEntity getTaskAddPermission() {
        return taskAddPermission;
    }

    public PermissionEntity getTaskEditPermission() {
        return taskEditPermission;
    }

    public PermissionEntity getTaskDeletePermission() {
        return taskDeletePermission;
    }

    public PermissionEntity getTaskProcessPermission() {
        return taskProcessPermission;
    }

    public PermissionEntity getTaskListAllPermission() {
        return taskListAllPermission;
    }

    public PermissionEntity getTaskListMyPermission() {
        return taskListMyPermission;
    }

    public PermissionEntity getTaskShowPermission() {
        return taskShowPermission;
    }
}
