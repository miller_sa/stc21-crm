package crm.stc21.reference;

import crm.stc21.entity.PermissionEntity;
import crm.stc21.repository.PermissionsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OutBoxPermissionsRef {
    PermissionsRepository permissionsRepository;

    private final PermissionEntity outBoxListAllPermission;
    private final PermissionEntity outBoxListLinkedInnerBoxPermission;
    private final PermissionEntity outBoxAddPermission;
    private final PermissionEntity outBoxEditPermission;
    private final PermissionEntity outBoxDeletePermission;

    @Autowired
    public OutBoxPermissionsRef(PermissionsRepository permissionsRepository) {
        this.permissionsRepository = permissionsRepository;
        this.outBoxListAllPermission = permissionsRepository.findByName("ROLE_OP_OUT_BOX_LIST_ALL");
        this.outBoxListLinkedInnerBoxPermission = permissionsRepository.findByName("ROLE_OP_OUT_BOX_LIST_LINKED_INNER_BOX");
        this.outBoxAddPermission = permissionsRepository.findByName("ROLE_OP_OUT_BOX_ADD");
        this.outBoxEditPermission = permissionsRepository.findByName("ROLE_OP_OUT_BOX_EDIT");
        this.outBoxDeletePermission = permissionsRepository.findByName("ROLE_OP_OUT_BOX_DELETE");

    }

    public PermissionEntity getOutBoxListAllPermission() {
        return outBoxListAllPermission;
    }

    public PermissionEntity getOutBoxListLinkedInnerBoxPermission() {
        return outBoxListLinkedInnerBoxPermission;
    }

    public PermissionEntity getOutBoxAddPermission() {
        return outBoxAddPermission;
    }

    public PermissionEntity getOutBoxEditPermission() {
        return outBoxEditPermission;
    }

    public PermissionEntity getOutBoxDeletePermission() {
        return outBoxDeletePermission;
    }
}
