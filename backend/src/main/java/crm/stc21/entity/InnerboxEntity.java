package crm.stc21.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.List;


@Entity
@Table(name = "innerbox", schema = "public")
public class InnerboxEntity {

    public InnerboxEntity() {
    }

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(generator = "innerbox_seq", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "innerbox_seq", allocationSize = 1, sequenceName = "innerbox_id_seq")
    private Long id;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "data_adoption", nullable = false)
    private LocalDate dataAdoption;

    @Column(name = "year", nullable = false)
    private Long year;

    @Column(name = "number", nullable = false)
    private Long number;

    @Column(name = "title", nullable = false, length = 500)
    private String title;

    @Column(name = "content", nullable = true)
    private String content;

    @Column(name = "created_at", nullable = true)
    private Timestamp createdAt;

    @Column(name = "updated_at", nullable = true)
    private Timestamp updatedAt;

    @Column(name = "is_deleted", nullable = true)
    private Boolean isDeleted;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "deadline", nullable = true)
    private LocalDate deadLine;

    public LocalDate getDataAdoption() {
        return dataAdoption;
    }

    public void setDataAdoption(LocalDate dataAdoption) {
        this.dataAdoption = dataAdoption;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    public LocalDate getDeadLine() {
        return deadLine;
    }

    public void setDeadLine(LocalDate deadLine) {
        this.deadLine = deadLine;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_created_id", nullable = false)
    private UserEntity userCreatedId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_updated_id", nullable = true)
    private UserEntity userUpdatedId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "type_doc_id", nullable = false)
    private InnerboxTypeNameEntity typeDocId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "author_id", nullable = false)
    private UserEntity authorId;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "taskInner")
    private List<TaskEntity> tasksInner;

    public InnerboxTypeNameEntity getTypeDocId() {
        return typeDocId;
    }

    public void setTypeDocId(InnerboxTypeNameEntity typeDocId) {
        this.typeDocId = typeDocId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getYear() {
        return year;
    }

    public void setYear(Long year) {
        this.year = year;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public UserEntity getUserCreatedId() {
        return userCreatedId;
    }

    public void setUserCreatedId(UserEntity userCreatedId) {
        this.userCreatedId = userCreatedId;
    }

    public UserEntity getUserUpdatedId() {
        return userUpdatedId;
    }

    public void setUserUpdatedId(UserEntity userUpdatedId) {
        this.userUpdatedId = userUpdatedId;
    }

    public UserEntity getAuthorId() {
        return authorId;
    }

    public void setAuthorId(UserEntity authorId) {
        this.authorId = authorId;
    }

    public List<TaskEntity> getTasksInner() {
        return tasksInner;
    }

    public void setTasksInner(List<TaskEntity> tasksInner) {
        this.tasksInner = tasksInner;
    }

}
