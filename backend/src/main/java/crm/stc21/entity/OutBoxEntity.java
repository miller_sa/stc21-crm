package crm.stc21.entity;


import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name="outbox", schema="public")
public class OutBoxEntity {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(generator = "outbox_seq", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "outbox_seq", allocationSize = 1, sequenceName = "outbox_id_seq")
    private Long id;

    @Column(name = "year", nullable = false)
    private Long year;

    @Column(name ="numb", nullable = false, length = 255)
    private String number;


    @Column (name = "inbox_number", nullable = true, length = 255)
    private String  inboxNumber;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "inbox_data", nullable = true)
    private LocalDate inboxDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "doc_date", nullable = false)
    private LocalDate docDate;

    @Column(name = "title", nullable = true, length = 400)
    private String title;

    @Column(name = "content", nullable = true)
    private String content;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_created_id",nullable = false)
    private UserEntity userCreatedId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_updated_id", nullable = true)
    private UserEntity userUpdatedId;

    @Column(name = "created_at", nullable = true)
    private Timestamp createdAt;

    @Column(name = "updated_at", nullable = true)
    private Timestamp updatedAt;

    @Column(name = "deleted_at", nullable = true)
    private Timestamp deletedAt;

    @Column(name = "is_deleted", nullable = true)
    private Boolean isDeleted;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="inbox_id", nullable = true)
    private IncomingBoxEntity inboxId;

    @Column(name = "type_of_outbox", nullable = true)
    private String typeOfOutbox;



    public OutBoxEntity(){}

    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Long getYear() {
        return year;
    }

    public void setYear(long year) {
        this.year = year;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getInboxNumber() {
        return inboxNumber;
    }

    public void setInboxNumber(String inboxNumber) {
        this.inboxNumber = inboxNumber;
    }

    public LocalDate getInboxDate() {
        return inboxDate;
    }

    public void setInboxDate(LocalDate inboxDate) {
        this.inboxDate = inboxDate;
    }

    public LocalDate getDocDate() {
        return docDate;
    }

    public void setDocDate(LocalDate docDate) {
        this.docDate = docDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public UserEntity getUserCreatedId() {
        return userCreatedId;
    }

    public void setUserCreatedId(UserEntity userCreatedId) {
        this.userCreatedId = userCreatedId;
    }

    public UserEntity getUserUpdatedId() {
        return userUpdatedId;
    }

    public void setUserUpdatedId(UserEntity userUpdatedId) {
        this.userUpdatedId = userUpdatedId;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Timestamp getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Timestamp deletedAt) {
        this.deletedAt = deletedAt;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    public IncomingBoxEntity getInboxId() {
        return inboxId;
    }

    public void setInboxId(IncomingBoxEntity inboxId) {
        this.inboxId = inboxId;
    }

    public String getTypeOfOutbox() {
        return typeOfOutbox;
    }

    public void setTypeOfOutbox(String typeOfOutbox) {
        this.typeOfOutbox = typeOfOutbox;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OutBoxEntity that = (OutBoxEntity) o;
        return id == that.id &&
                year == that.year &&
                number.equals(that.number) &&
                Objects.equals(inboxNumber, that.inboxNumber) &&
                Objects.equals(inboxDate, that.inboxDate) &&
                docDate.equals(that.docDate) &&
                Objects.equals(title, that.title) &&
                Objects.equals(content, that.content) &&
                userCreatedId.equals(that.userCreatedId) &&
                Objects.equals(userUpdatedId, that.userUpdatedId) &&
                Objects.equals(createdAt, that.createdAt) &&
                Objects.equals(updatedAt, that.updatedAt) &&
                Objects.equals(deletedAt, that.deletedAt) &&
                Objects.equals(isDeleted, that.isDeleted) &&
                Objects.equals(inboxId, that.inboxId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, year, number, inboxNumber, inboxDate, docDate, title,
                content, userCreatedId, userUpdatedId, createdAt, updatedAt, deletedAt, isDeleted, inboxId);
    }
}
