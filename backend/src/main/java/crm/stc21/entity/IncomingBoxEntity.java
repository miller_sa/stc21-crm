package crm.stc21.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "incomingbox", schema = "public")
public class IncomingBoxEntity {

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(generator = "incomingbox_seq", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "incomingbox_seq", allocationSize = 1, sequenceName = "incomingbox_id_seq")
    private Long id;

    @Column(name = "year", nullable = false)
    private Long year;

    @Column(name = "numb", nullable = false)
    private Long number;

    @Column(name = "out_numb", nullable = false, length = 255)
    private String outNumb;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "taskIncom")
    private List<TaskEntity> taskIncoming;

    public List<TaskEntity> getTaskIncoming() {
        return taskIncoming;
    }

    public void setTaskIncoming(List<TaskEntity> taskIncoming) {
        this.taskIncoming = taskIncoming;
    }

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "doc_date", nullable = false)
    private LocalDate docDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "out_date", nullable = false)
    private LocalDate outDate;

    @Column(name = "title", nullable = false, length = 400)
    private String title;

    @Column(name = "content", nullable = false)
    private String content;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_updated_id")
    private UserEntity userUpdated;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_created_id")
    private UserEntity userCreated;

    @Column(name = "created_at", nullable = true)
    private Timestamp createdAt;

    @Column(name = "updated_at", nullable = true)
    private Timestamp updatedAt;

    @Column(name = "deleted_at", nullable = true)
    private Timestamp deletedAt;

    @Column(name = "is_deleted", nullable = true)
    private Boolean isDeleted;

    @OneToOne(mappedBy = "inboxId")
    private OutBoxEntity outBox;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "deadline", nullable = true)
    private LocalDate deadLine;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "type_doc_id", nullable = false)
    private InnerboxTypeNameEntity typeDocId;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "data_adoption", nullable = false)
    private LocalDate dataAdoption;


    public InnerboxTypeNameEntity getTypeDocId() {
        return typeDocId;
    }

    public void setTypeDocId(InnerboxTypeNameEntity typeDocId) {
        this.typeDocId = typeDocId;
    }

    public LocalDate getDeadLine() {
        return deadLine;
    }

    public void setDeadLine(LocalDate deadLine) {
        this.deadLine = deadLine;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getYear() {
        return year;
    }

    public void setYear(Long year) {
        this.year = year;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public String getOutNumb() {
        return outNumb;
    }

    public void setOutNumb(String outNumb) {
        this.outNumb = outNumb;
    }

    public LocalDate getDocDate() {
        return docDate;
    }

    public void setDocDate(LocalDate docDate) {
        this.docDate = docDate;
    }

    public LocalDate getOutDate() {
        return outDate;
    }

    public void setOutDate(LocalDate outDate) {
        this.outDate = outDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Timestamp getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Timestamp deletedAt) {
        this.deletedAt = deletedAt;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    public UserEntity getUserUpdated() {
        return userUpdated;
    }

    public void setUserUpdated(UserEntity userUpdated) {
        this.userUpdated = userUpdated;
    }

    public UserEntity getUserCreated() {
        return userCreated;
    }

    public void setUserCreated(UserEntity userCreated) {
        this.userCreated = userCreated;
    }

    public LocalDate getDataAdoption() {
        return dataAdoption;
    }

    public void setDataAdoption(LocalDate dataAdoption) {
        this.dataAdoption = dataAdoption;
    }

}