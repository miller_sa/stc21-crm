package crm.stc21.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;


@Entity
@Table(name = "innerbox_type_name", schema = "public")
public class InnerboxTypeNameEntity {

    public InnerboxTypeNameEntity() {
    }

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name", nullable = true, length = 50)
    private String name;

    @Column(name = "display_name", nullable = false, length = 250)
    private String displayName;

    @OneToMany(mappedBy = "typeDocId",cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<InnerboxEntity> innerDocs;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public Set<InnerboxEntity> getInnerDocs() {
        return innerDocs;
    }

    public void setInnerDocs(Set<InnerboxEntity> innerDocs) {
        this.innerDocs = innerDocs;
    }
}
