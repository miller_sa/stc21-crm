FROM openjdk:8-jdk-alpine
VOLUME /tmp
ARG JAR_FILE=backend/target/crm.jar
ADD ${JAR_FILE} crm.jar
ENTRYPOINT ["java","-jar","/crm.jar"]