ALTER TABLE public.tasks
  ADD COLUMN task_user_id INTEGER;

ALTER TABLE public.tasks
  ADD CONSTRAINT tasks_fk_task_user_id FOREIGN KEY (task_user_id)
    REFERENCES public.users(id)
    ON DELETE RESTRICT
    ON UPDATE NO ACTION
    NOT DEFERRABLE;