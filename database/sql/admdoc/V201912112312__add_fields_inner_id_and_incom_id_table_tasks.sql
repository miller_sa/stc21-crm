ALTER TABLE public.tasks

add column inner_id        integer
        constraint fk_tasks_innerbox_id
            references innerbox
            on delete restrict,
   add column incoming_id     integer
                   constraint fk_tasks_incomingbox_id
                   references incomingbox
               on delete restrict;

comment on column tasks.inner_id is 'id входящего документа';

comment on column tasks.incoming_id is 'id исходящего документа';
