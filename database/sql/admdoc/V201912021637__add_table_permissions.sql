CREATE TABLE public.permissions (
  id SERIAL NOT NULL,
  name VARCHAR(255) NOT NULL UNIQUE,
  display_name VARCHAR(255) NOT NULL UNIQUE,
  created_at TIMESTAMP WITHOUT TIME ZONE,
  updated_at TIMESTAMP(0) WITHOUT TIME ZONE,
  PRIMARY KEY(id)
) ;

ALTER TABLE public.permissions
  ALTER COLUMN id SET STATISTICS 0;

ALTER TABLE public.permissions
  ALTER COLUMN name SET STATISTICS 0;

ALTER TABLE public.permissions
  ALTER COLUMN display_name SET STATISTICS 0;

COMMENT ON COLUMN public.permissions.id
IS 'Идентификатор';

COMMENT ON COLUMN public.permissions.name
IS 'Имя';

COMMENT ON COLUMN public.permissions.display_name
IS 'Отображаемое имя';

COMMENT ON COLUMN public.permissions.created_at
IS 'Дата создания';

COMMENT ON COLUMN public.permissions.updated_at
IS 'Дата последнего обновления';
