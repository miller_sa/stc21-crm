DELETE FROM public.incomingBox;
DELETE FROM public.tasks;
DELETE FROM public.task_statuses;
DELETE FROM public.users;
DELETE FROM public.role_permissions;
DELETE FROM public.permissions;
DELETE FROM public.roles;
DELETE FROM public.innerbox;
DELETE FROM public.outbox;
DELETE FROM public.innerbox_type_name;


INSERT INTO public.roles ("id", "name", "display_name", "description", "created_at", "updated_at")
VALUES
  (1, 'ROLE_TASK_MANAGER', 'Менеджер задач', NULL, current_timestamp, current_timestamp),
  (2, 'ROLE_ADMINISTRATOR', 'Администратор', NULL, current_timestamp, current_timestamp),
  (3, 'ROLE_TASK_PERFORMER', 'Исполнитель', NULL, current_timestamp, current_timestamp);

ALTER SEQUENCE public.roles_id_seq
  INCREMENT 1 MINVALUE 1
  MAXVALUE 2147483647 START 1
  RESTART 4 CACHE 1
  NO CYCLE;


INSERT INTO public.permissions ("id", "name", "display_name", "created_at", "updated_at", "error_message")
VALUES
  (2,  'ROLE_OP_TASK_ADD', 'Добавление новых задач', current_timestamp, NULL, 'Нет доступа для добавления новых задач.'),
  (5,  'ROLE_OP_TASK_EDIT', 'Редактирование задач', current_timestamp, NULL, 'Нет доступа для редактирования задачи.'),
  (6,  'ROLE_OP_TASK_PROCESS', 'Исполнение задач', current_timestamp, NULL, 'Нет доступа для выполнения задачи.'),
  (14, 'ROLE_OP_TASK_DELETE', 'Удаление задачи', current_timestamp, NULL, 'Нет доступа для удаления задачи.'),
  (27, 'ROLE_OP_TASK_SHOW', 'Просмотр задачи', current_timestamp, NULL, 'Нет доступа для просмотра задачи.'),
  (7,  'ROLE_OP_TASK_LIST_ALL', 'Просмотр всех задач', current_timestamp, NULL, 'Нет доступа для просмотра всех задач'),
  (8,  'ROLE_OP_TASK_LIST_MY', 'Просмотр своих задач', current_timestamp, NULL, 'Нет доступа для просмотра задач.'),
  (9,  'ROLE_OP_USER_EDIT_PERMISSIONS', 'Назначение прав пользователям', current_timestamp, NULL, 'Нет доступа для назначения прав пользователю.'),
  (28, 'ROLE_OP_USER_HAS_CHANGE_HIS_SETTINGS', 'Пользователь может менять свои настройки', current_timestamp, NULL, 'Нет доступа для смены настроек.'),
  (10, 'ROLE_OP_INCOMING_BOX_LIST_ALL', 'Просмотр всей входящей почты', current_timestamp, NULL, 'Нет доступа для просмотра входящей почты.'),
  (11, 'ROLE_OP_INCOMING_BOX_LIST_LINKED_TASKS', 'Просмотр связанных задач', current_timestamp, NULL, 'Нет доступа для просмотра связанных задач.'),
  (12, 'ROLE_OP_INCOMING_BOX_ADD', 'Добавление входящей почты', current_timestamp, NULL, 'Нет доступа для добавления входящей почты.'),
  (13, 'ROLE_OP_INCOMING_BOX_EDIT', 'Изменение входящей почты', current_timestamp, NULL, 'Нет доступа для изменения входящей почты.'),
  (15, 'ROLE_OP_INCOMING_BOX_DELETE', 'Удаление входящей почты', current_timestamp, NULL, 'Нет доступа удаления входящей почты.'),
  (16, 'ROLE_OP_OUT_BOX_LIST_ALL', 'Просмотр всей исходящей почты', current_timestamp, NULL, 'Нет доступа для просмотра исходящей почты.'),
  (17, 'ROLE_OP_OUT_BOX_LIST_LINKED_INNER_BOX', 'Просмотр связанных входящих', current_timestamp, NULL, 'Нет доступа.'),
  (18, 'ROLE_OP_OUT_BOX_ADD', 'Добавление исходящей почты', current_timestamp, NULL, 'Нет доступа добавления исходяшей почты.'),
  (19, 'ROLE_OP_OUT_BOX_EDIT', 'Изменение исходящей почты', current_timestamp, NULL, 'Нет доступа для изменения исходязей почты.'),
  (20, 'ROLE_OP_OUT_BOX_DELETE', 'Удаление исходящей почты', current_timestamp, NULL, 'Нет доступа для удаления исходящей почты.'),
  (21, 'ROLE_OP_INNER_BOX_LIST_ALL', 'Просмотр всех внутренних документов', current_timestamp, NULL, 'Нет доступа для просмотра внутренних документов.'),
  (22, 'ROLE_OP_INNER_BOX_LIST_LINKED_TASKS', 'Просмотр связанных задач', current_timestamp, NULL, 'Нет доступа для просмотра связанных задач.'),
  (23, 'ROLE_OP_INNER_BOX_ADD', 'Добавление внутренних документов', current_timestamp, NULL, 'Нет доступа для добавления внутренних документов.'),
  (24, 'ROLE_OP_INNER_BOX_EDIT', 'Изменение внутренних документов', current_timestamp, NULL, 'Нет доступа для изменения внутренних документов'),
  (25, 'ROLE_OP_INNER_BOX_DELETE', 'Удаление внутренних документов', current_timestamp, NULL, 'Нет доступа для удаления внутренних документов.');



ALTER SEQUENCE public.permissions_id_seq
  INCREMENT 1 MINVALUE 1
  MAXVALUE 2147483647 START 1
  RESTART 29 CACHE 1
  NO CYCLE;


INSERT INTO public.role_permissions ("role_id", "permission_id")
VALUES
  (1, 2),
  (1, 5),
  (1, 6),
  (1, 14),
  (1, 27),
  (1, 7),
  (1, 8),
  (1, 10),
  (1, 11),
  (1, 12),
  (1, 13),
  (1, 15),
  (1, 16),
  (1, 17),
  (1, 18),
  (1, 19),
  (1, 20),
  (1, 21),
  (1, 22),
  (1, 23),
  (1, 24),
  (1, 25),
  (3, 6),
  (3, 8),
  (3, 27);

-- For ADMINISTRATOR, ALL permissions, excluding: ROLE_OP_USER_HAS_CHANGE_HIS_SETTINGS, ROLE_OP_USER_EDIT_PERMISSIONS
INSERT INTO public.role_permissions
SELECT 2, id
FROM permissions
WHERE (NOT id in (28));

INSERT INTO public.users ("id", "login", "firstname", "lastname", "fathername", "email", "password", "created_at", "updated_at", "telegram_username", "role_id", "is_active")
VALUES
  (1, 'IvanovaII', 'Ирина', 'Иванова', 'Ивановна', 'ivanovaii@beldashki.ru', '123', current_timestamp, NULL, NULL, 1, true),
  (2, 'Admin', 'Пётр', 'Петров', 'Петрович', 'petrovpp@beldashki.ru', '123', current_timestamp, NULL, NULL, 2, true),
  (3, 'SemenovSS', 'Семён', 'Семёнов', 'Семёнович', 'semenovss@beldashki.ru', '123', current_timestamp, NULL, NULL, 3, true);
--   (4, 't', 'TestN', 'TestLN', 'TestFN', 'testtt@beldashki.ru', 't', current_timestamp, NULL, NULL, 2, true),
--   (5, 'u', 'UserU', 'UserLN', 'UserFN', 'useruu@beldashki.ru', 'u', current_timestamp, NULL, NULL, 3, true);

ALTER SEQUENCE public.users_id_seq
  INCREMENT 1 MINVALUE 1
  MAXVALUE 2147483647 START 1
  RESTART 6 CACHE 1
  NO CYCLE;


INSERT INTO public.task_statuses ("id", "name", "display_name")
VALUES
  (1, 'ST_DRAFT', 'Черновик'),
  (2, 'ST_ASSIGNED', 'Назначена'),
  (3, 'ST_IN_WORK', 'В работе'),
  (4, 'ST_DONE', 'Выполнена'),
  (5, 'ST_REJECTED', 'Отклонена'),
  (6, 'ST_DELETED', 'Удалена');

ALTER SEQUENCE public.task_statuses_id_seq
  INCREMENT 1 MINVALUE 1
  MAXVALUE 2147483647 START 1
  RESTART 7 CACHE 1
  NO CYCLE;



INSERT INTO public.tasks (id, title, description, task_status_id, is_accepted, accept_time, user_created_id, user_updated_id, user_deleted_id, created_at, updated_at, deleted_at, is_deleted, task_user_id)
VALUES
  (1, 'Бизнес план январь 2020', 'Подготовить бизнесплано на январь 2020 по материалам, услугам', 1, False, NULL, 1, NULL, NULL, current_timestamp,current_timestamp, NULL, False, 1),
  (2, 'Отчет по материалам декабрь 2019', 'Подготовить отчет по использованным материалам за декабрь 2019', 2, False, NULL, 1, NULL, NULL, current_timestamp, current_timestamp, NULL, False, 1),
  (3, 'Отчет по закупкам декабрь 2019', 'Подготовить отчет по закупкам декабрь 2019', 3, False, NULL, 1, NULL, NULL, current_timestamp, current_timestamp, NULL, False, 3),
  (4, 'Тендер по закупке лицензий KAV', 'Подготовить техническое задание для проведения тендера на закупку KAV', 4, False, NULL, 1, 3, NULL, current_timestamp, current_timestamp, NULL, False, 3),
  (5, 'Сдать документы в бухгалтерию', 'Сдать акты выполненных работы по прокладке оптоволокна в бухгалтерию', 5, False, NULL, 1, 3, NULL, current_timestamp, current_timestamp, NULL, False, 3),
  (6, 'Выбор поставщика', 'По итогам проведенного тендара на закупку серверного оборудования подготовить документы о выборе поставщика', 5, False, NULL, 1, 3, NULL, current_timestamp, current_timestamp, NULL, False, 3),
  (7, 'Списание оборудования', 'Подготовить перечень оборудования для списания в январе 2020', 1, False, NULL, 1, 3, NULL, current_timestamp, current_timestamp, NULL, False, 3);

ALTER SEQUENCE public.tasks_id_seq
  INCREMENT 1 MINVALUE 1
  MAXVALUE 2147483647 START 1
  RESTART 8 CACHE 1
  NO CYCLE;
INSERT INTO public.outbox (id, year, numb, inbox_number, inbox_data, doc_date, title, content, user_created_id, user_updated_id, created_at, updated_at, deleted_at, is_deleted, inbox_id, type_of_outbox)
 VALUES
        (1, 2019, '62', '255', '1970-01-07', '2019-12-07', 'Письмо в ИФНС', 'Ответ на письмо от 20.11.2019', 1, 1, '2019-12-09 12:00:45.000000', null, null, false, 1, 'Письмо'),
        (2, 2019, '63', '256', '2019-12-08', '2019-12-08', 'Участие в тендере', 'Предлагаем Вам принять участие в тендере на поставку прошраммного обеспечения', 2, null, '2019-12-09 12:02:31.000000', null, null, false, 2, 'Письмо');

ALTER SEQUENCE public.outbox_id_seq
  INCREMENT 1 MINVALUE 1
  MAXVALUE 2147483647 START 1
  RESTART 3 CACHE 1
  NO CYCLE;
INSERT INTO public.innerbox_type_name (id, name, display_name)
VALUES
       (1, 'post', 'Постановление'),
       (2, 'rasp', 'Распоряжение'),
       (3, 'prikaz', 'Приказ'),
       (4, 'kom', 'Командировочное'),
       (5, 'pismo', 'Письмо');

INSERT INTO public.innerbox (id, data_adoption, year, number, type_doc_id, title, content, created_at, updated_at, is_deleted, user_created_id, user_updated_id, deadline, author_id)
VALUES
       (1, '2019-12-10', 2019, 54, 1, 'О проведении социологического опроса об уровне восприятия коррупции', null, '2019-12-10 10:58:38.000000', null, false, 1, null, '2019-12-13', 1),
       (2, '2019-12-10', 2019, 105, 2, 'О создании рабочей комиссии', null, '2019-12-10 10:59:15.000000', null, false, 1, null, '2019-12-20', 1);

ALTER SEQUENCE public.innerbox_id_seq
    INCREMENT 1 MINVALUE 1
        MAXVALUE 2147483647 START 1
        RESTART 3 CACHE 1
        NO CYCLE;

INSERT INTO public.incomingBox ("id", "year", "numb", "out_numb", "doc_date", "out_date", "title", "content", "user_created_id", "user_updated_id", "created_at", "updated_at", "deleted_at", "is_deleted", "type_doc_id", "deadline", "data_adoption")
VALUES
(1, 2019, '101', '998/11/123', '2019-12-02', '2019-11-30', 'Комерческое предложение', 'Комерческое предложение на приобретение лицензий Kaspersky Antivirus (200 пользователей, 1 год)', 1, NULL, current_timestamp, NULL, NULL, False, 1, '2019-12-22', '2019-12-10'),
(2, 2019, '102', '566', '2019-12-02', '2019-11-26', 'Подтверждение оплаты', 'Подтверждение оплаты за блоки питания', 1, NULL, current_timestamp, NULL, NULL, False, 2, '2019-12-24', '2019-12-10'),
(3, 2019, '103', '799/К', '2019-12-02', '2019-11-27', 'Информация ИФНС', 'Информация по заполнению декларации по акцизам', 1, NULL, current_timestamp, NULL, NULL, False, 1, '2019-12-25', '2019-12-10');

ALTER SEQUENCE public.incomingBox_id_seq
    INCREMENT 1 MINVALUE 1
        MAXVALUE 2147483647 START 1
        RESTART 4 CACHE 1
        NO CYCLE;