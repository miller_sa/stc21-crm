create table public.innerbox
(
    id               serial       not null
        constraint innerbox_pk
            primary key,
    data_adoption    date         not null,
    year             integer      not null,
    number           integer      not null,
    type_doc_id      integer      not null,
    title            varchar(500) not null,
    content          text,
    created_at       timestamp,
    updated_at       timestamp,
    is_deleted       boolean default false,
    user_created_id  integer      not null
        constraint fk_innerbox_user_created_id
            references public.users
            on delete restrict,
    user_updated_id  integer
        constraint fk_innerbox_user_updated_id
            references public.users
            on delete restrict,
    deadline         date,
    author_id        integer      not null
        constraint fk_innerbox_author_id
            references public.users
            on delete restrict
);

comment on table public.innerbox is 'внутренние документы';

comment on column public.innerbox.data_adoption is 'дата принятия';

comment on column public.innerbox.year is 'год';

comment on column public.innerbox.number is 'номер внутреннего  документа';

comment on column public.innerbox.type_doc_id is 'тип документа';

comment on column public.innerbox.title is 'название документа';

comment on column public.innerbox.content is 'содержание';

comment on column public.innerbox.is_deleted is 'флаг true - документ удалили';

comment on column public.innerbox.user_created_id is 'пользователь добавивший документ';

comment on column public.innerbox.user_updated_id is 'пользователь внес изменения в документ';

comment on column public.innerbox.deadline is 'дата контроля';

comment on column public.innerbox.author_id is 'автор';


