ALTER TABLE public.task_statuses
    ADD CONSTRAINT task_statuses_idx_uk_name
        UNIQUE (name) NOT DEFERRABLE;