CREATE TABLE public.role_permissions (
  role_id INTEGER NOT NULL,
  permission_id INTEGER NOT NULL
) ;

ALTER TABLE public.role_permissions
  ADD CONSTRAINT role_permissions_idx 
    PRIMARY KEY (role_id, permission_id) NOT DEFERRABLE;
    
ALTER TABLE public.role_permissions
  ADD CONSTRAINT role_permissions_fk_role_id FOREIGN KEY (role_id)
    REFERENCES public.roles(id)
    ON DELETE RESTRICT
    ON UPDATE NO ACTION
    NOT DEFERRABLE;

ALTER TABLE public.role_permissions
  ADD CONSTRAINT role_permissions_fk_permission_id FOREIGN KEY (permission_id)
    REFERENCES public.permissions(id)
    ON DELETE RESTRICT
    ON UPDATE NO ACTION
    NOT DEFERRABLE;
