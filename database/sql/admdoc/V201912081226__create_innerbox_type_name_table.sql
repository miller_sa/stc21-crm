create table innerbox_type_name
(
    id serial not null,
    name varchar(50),
    display_name varchar(250) not null
);

comment on table innerbox_type_name is 'наименование типов внут. документов';

comment on column innerbox_type_name.name is 'внутренние именование типа';

comment on column innerbox_type_name.display_name is 'именование типа док-та для отображения';

create unique index type_name_display_name_uindex
    on innerbox_type_name (display_name);

create unique index type_name_id_uindex
    on innerbox_type_name (id);

create unique index type_name_name_uindex
    on innerbox_type_name (name);

alter table innerbox_type_name
    add constraint type_name_pk
        primary key (id);
alter table innerbox
    add constraint fk_innerbox_type_doc_id
        foreign key (type_doc_id) references innerbox_type_name
            on delete restrict;

