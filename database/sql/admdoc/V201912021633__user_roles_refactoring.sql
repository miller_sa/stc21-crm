ALTER TABLE users ADD role_id INTEGER;

ALTER TABLE public.users
  ADD CONSTRAINT users_fk_role_id FOREIGN KEY (role_id)
    REFERENCES public.roles(id)
    ON DELETE RESTRICT
    ON UPDATE NO ACTION
    NOT DEFERRABLE;

DROP TABLE IF EXISTS user_roles;