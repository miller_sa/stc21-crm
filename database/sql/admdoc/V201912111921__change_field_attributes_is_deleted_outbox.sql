alter table public.outbox alter column is_deleted set not null;

alter table public.outbox alter column is_deleted set default false;