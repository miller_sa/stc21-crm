ALTER TABLE public.incomingbox
    ADD COLUMN type_doc_id INTEGER not null,
    ADD COLUMN deadline date,
    ADD COLUMN data_adoption date,
    ADD CONSTRAINT fk_incomingbox_type_doc_id
foreign key (type_doc_id) references innerbox_type_name
on delete restrict;

comment on column public.incomingbox.type_doc_id is 'тип документа';
comment on column public.incomingbox.deadline is 'дата контроля';
comment on column public.incomingbox.data_adoption is 'дата принятия';

