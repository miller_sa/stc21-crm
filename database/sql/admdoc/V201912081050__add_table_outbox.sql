create table outbox
(
    id              serial              not null
        constraint outbox_pk
            primary key,
    year            integer             not null,
    numb            varchar default 255 not null,
    inbox_number    varchar default 255,
    inbox_data      date,
    doc_date        date                not null,
    title           varchar default 400,
    content         text,
    user_created_id integer             not null
        constraint outbox___fk_created_user_id
            references users
            on delete restrict,
    user_updated_id integer
        constraint outbox___fk_user_updated_id
            references users
            on delete restrict,
    created_at      timestamp,
    updated_at      timestamp,
    deleted_at      timestamp,
    is_deleted      boolean,
    inbox_id        integer
);

comment on table outbox is 'Исходящая концелярия';

comment on column outbox.id is 'Идентификатор';

comment on column outbox.year is 'Год';

comment on column outbox.numb is 'Номер исходящего';

comment on column outbox.inbox_number is 'Входящий номер';

comment on column outbox.inbox_data is 'Дата входящего';

comment on column outbox.doc_date is 'Дата исходящего';

comment on column outbox.title is 'Заголовок';

comment on column outbox.content is 'Содержание';

comment on column outbox.user_created_id is 'Кем создано';

comment on column outbox.user_updated_id is 'Кем обновлено';

comment on column outbox.created_at is 'Дата создания';

comment on column outbox.updated_at is 'Дата обновления';

comment on column outbox.deleted_at is 'Дата удаления';

comment on column outbox.is_deleted is 'Пометка на удаление';

comment on column outbox.inbox_id is 'Идентификатор входящего документа';



