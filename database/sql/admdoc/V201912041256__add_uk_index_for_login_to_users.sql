ALTER TABLE public.users
    ADD CONSTRAINT users_idx_uk_login
        UNIQUE (login) NOT DEFERRABLE;